24-11-2020
created by SamRaskul@gmail.com

This website created for Luda.

it has 5 index pages:
1- https://luda.nuxt.info/ //this is home page
2- https://luda.nuxt.info/about //this is about page
3- https://luda.nuxt.info/clients (if you logged in, you will redirect to https://luda.nuxt.info/clients/ticket for clients area)(it has sign_up and login form inside)
4- vacancies: https://luda.nuxt.info/vacancies // in new update changed to /info-platform
5- contact: https://luda.nuxt.info/contact (everybody can send message to admin without file attachment)

it has 3 client pages so after clients logged_in to the website can see these 3 pages:
1- https://luda.nuxt.info/clients/ticket clients can check all the messages that they sent to admin.
2- https://luda.nuxt.info/clients/ticket/new (clients can send message to admin without file attachments)
3- https://luda.nuxt.info/clients/ticket/{id_of_ticket} (clients can see their own messages ans continue chat with admin with file attachments)

it has admin panel with 5 pages and just admin can see them:
1- https://luda.nuxt.info/admin //see the admin report
2- https://luda.nuxt.info/admin/message //see all the messages and statuses of messages
3- https://luda.nuxt.info/admin/users //see all the users and users_info
4- https://luda.nuxt.info/admin/messages // advanced settings for find and edit or delete a message
5- https://luda.nuxt.info/admin/settings // advanced settings for change the translation of words
--------------------------------------------------
admin email: luda@luda.com
admin password: MGF2t6XPE9BGRcjs
--------------------------------------------------
I used settings table and 2 helper functions 
translateAllWordsFromTitle() -> to put all the settings data in an array
translateContent() -> to translate content
--------------------------------------------------
There is one job to send to email
you can find options about email in 
https://luda.nuxt.info/admin/notifications
--------------------------------------------------
There a helper function to edit Gmail settings
so admin can change the Gmail setting in admin panel
--------------------------------------------------


====================================Start How to Deploy This App====================================
------------------------------------------------start project infos--------
# Laravel Framework version 8.15.0
------------------------------------------------end project infos--------

------------------------------------------------start server infos--------
# ubuntu server version 20
# PHP version 7.4.13 (php-fpm7.4 used in the service for [Nginx config file] and [laravel queue workers])//php 8.0 is not supported by yajra-datatable package
# nginx version 1.19.0
# mysql version 8.0.22
------------------------------------------------end server infos--------

------------------------------------------------start mysql trigger [code]--------
--I used this trigger in database:--

CREATE DEFINER=`root`@`localhost` TRIGGER `users_after_insert` AFTER INSERT ON `users` FOR EACH ROW BEGIN
insert into user_infos(user_id, created_at, updated_at)
values (NEW.id, now(), now());
END
I use it to make one to one relationship between users and user_infos tables.
it's useable for data table in http://luda.nuxt.info/admin/users
so if you search for a user, you will not get any error. 
------------------------------------------------end mysql trigger [code]--------

------------------------------------------------start nginx config [code]--------
--/etc/nginx/conf.d/luda.nuxt.info.conf--
--with cloudflare.com we can enable https--

server {
    listen 80;
    server_name luda.nuxt.info;
    root /home/luda.nuxt.info/public;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    index index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
        fastcgi_pass unix:/run/php/php7.4-fpm.sock;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME $fastcgi_script_name;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}
------------------------------------------------end nginx config [code]--------

------------------------------------------------start systemctl [code]--------
--this project needs systemctl for emails follow this instruction--

# Laravel queue worker using systemd
# ----------------------------------
#
# /lib/systemd/system/queue-main.service
#
# run this command to enable service:
# systemctl enable queue-main.service

[Unit]
Description=Laravel queue worker

[Service]
User=www-data
Group=www-data
Restart=always
ExecStart=/usr/bin/php7.4 /home/luda.nuxt.info/artisan queue:work --sleep=5 --tries=3 --delay=21600 --timeout=216000
Nice=19

[Install]
WantedBy=multi-user.target
------------------------------------------------end systemctl [code]--------

------------------------------------------------start local email configuration (no needed)--------
MAIL_MAILER=smtp
MAIL_HOST=smtp.googlemail.com
MAIL_PORT=25
MAIL_USERNAME=sadeghsadegh21@gmail.com
MAIL_PASSWORD=sadegh69Sadegh71
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=sadeghsadegh21@gmail.com
MAIL_FROM_NAME="${APP_NAME}"
------------------------------------------------end local email configuration (no needed)--------

------------------------------------------------start production email configuration [code]--------
MAIL_MAILER=smtp
MAIL_HOST=smtp.googlemail.com
MAIL_PORT=465
MAIL_USERNAME=sadeghsadegh21@gmail.com
MAIL_PASSWORD=YOUREMAILPASSWORD
MAIL_ENCRYPTION=ssl
MAIL_FROM_ADDRESS=sadeghsadegh21@gmail.com
MAIL_FROM_NAME="${APP_NAME}"
------------------------------------------------end production email configuration [code]--------

------------------------------------------------start .env configuration [code]--------
APP_NAME=Laravel
APP_ENV=production
APP_KEY=base64:CHfRzlQjvrpxngvItOo16YzHWoHCoF/iGtbpeAG7ykA=
APP_DEBUG=false
APP_URL=https://luda.nuxt.info
------------------------------------------------end .env configuration [code]--------

====================================End How to Deploy This App====================================
