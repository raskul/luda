<?php

use App\Http\Controllers\Admin\AdminDashboardController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FunctionsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotifyController;
use App\Jobs\EmailTo;
use App\Mail\NotifyUser;
use App\Models\Notify;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/language/{lang}', [FunctionsController::class, 'language'])->name('language');

Route::get('/about', [HomeController::class, 'about'])->name('about');

Route::get('/info-platform', [HomeController::class, 'vacancies'])->name('vacancies');

Route::get('/contact', [HomeController::class, 'contact'])->name('contact');

Route::group(['prefix' => 'admin', 'middleware' => ['isAdmin']], function () {
    Route::get('/', [AdminDashboardController::class, 'index'])->name('admin');
    Route::get('/message', [AdminDashboardController::class, 'message'])->name('admin.message');
    Route::post('/message/changeStatus/{id}', [AdminDashboardController::class, 'messageChangeStatus'])->name('admin.message.changeStatus');
    Route::get('/message/reply/{id}', [AdminDashboardController::class, 'messageReplyGet'])->name('admin.message.reply.get');
    Route::get('/notifications', [NotifyController::class, 'showForm'])->name('notificationsShowForm');
    Route::post('/notifications', [NotifyController::class, 'showFormPost'])->name('notificationsShowFormPost');

    Route::resource('users', App\Http\Controllers\Admin\UserController::class, ["as" => 'admin']);
    Route::resource('settings', App\Http\Controllers\Admin\SettingController::class, ["as" => 'admin']);
    Route::resource('messages', App\Http\Controllers\Admin\MessageController::class, ["as" => 'admin']);
});

Route::post('/message/store', [HomeController::class, 'messageStore'])->name('message.sore');

Route::get('/clients', [HomeController::class, 'clients'])->name('clients');
Route::get('/forgetPassword', [HomeController::class, 'forgetPassword'])->name('forgetPassword');

Route::get('/clients/ticket', [DashboardController::class, 'clientsTicket'])->middleware('auth')->name('clients.ticket');
Route::get('/clients/ticket/new', [DashboardController::class, 'clientsTicketNewGet'])->middleware('auth')->name('clients.ticket.new.get');
Route::post('/clients/ticket/new', [DashboardController::class, 'clientsTicketNewPost'])->middleware('auth')->name('clients.ticket.new.post');
Route::post('/clients/ticket/answer/{id}', [DashboardController::class, 'clientsTicketAnswerPost'])->middleware('auth')->name('clients.ticket.answer.post');
Route::get('/clients/ticket/{id}', [DashboardController::class, 'clientsTicketSingle'])->where('id', '[0-9]+')->middleware('auth')->name('clients.ticket.single');

Route::get('/test', function () {
    $n = new Notify;
    $adminEmail = $n->adminEmail();

    //////////////////////////////////////////////////////
    // EmailTo::dispatch($adminEmail, 'New message subject' , 'test body message');
    //////////////////////////////////////////////
    $notifyUser = new NotifyUser;
    $notifyUser->text = '$this->text';
    $notifyUser->subject = '$this->subject';

    $user = new User;
    $user->id = 66666;
    $user->email = $adminEmail;
    $user->name = 'sssss';
    $user->email_verified_at = now();
    $user->password = '$2y$10$EKFhfDnn/4goo8yAkgb3j./xWpGlKREzHI.pDIA/KrZMyURcDrXi2';
    $user->created_at = now();
    $user->updated_at = now();

    Mail::to($user)->queue($notifyUser);
    /////////////////////////////
    return 'message sent';
});

