let mobileMenuBtn = document.getElementById('mobile-lists');
let menuLists = document.getElementById('lists');
let containerDivLayout = document.querySelector('.container');

let toggleMobileMenuStatus = false;

mobileMenuBtn.addEventListener('click', function () { 
    if (toggleMobileMenuStatus) {
        menuLists.style.display = 'none';
        toggleMobileMenuStatus = false;
        containerDivLayout.style = "grid-template: 150px auto 30px / auto";

    } else {
        menuLists.style.display = 'grid';
        toggleMobileMenuStatus = true;
        containerDivLayout.style = "grid-template: auto auto 30px / auto";
    }
});

