@extends('layouts.layout')
@section('fullPage')
    

@include('partials.formStyle')


    @push('styles')
        <style>
            /* .titleContent {
                display: flex;

            }

            .titleContent img {
                margin-left: 60px;
            } */

        </style>
    @endpush

    {{-- <div class="titleContent"> --}}
        {{-- <img src="/assets/images/headlineBalls.png" alt=""> --}}
    {{-- </div> --}}
    <div style="width: 100%">
        <div class="form">

            <ul class="tab-group">
                <li class="tab active"><a href="{{ route('clients') }}">Return back</a></li>
            </ul>

            <div class="tab-content">
                <div id="signup">
                    <h1>Recover Your Password</h1>

                    <form method="post" action="{{ url('/password/reset') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="field-wrap">
                            <input name="email" placeholder="Email Address" type="email" required autocomplete="off" />
                        </div>
                        <div class="field-wrap">
                            <input name="password" placeholder="Password" type="password" required autocomplete="off" />
                        </div>
                        <div class="field-wrap">
                            <input name="password_confirmation" placeholder="Retype password" type="password" required autocomplete="off" />
                        </div>

                        <button type="submit" class="button button-block">Recover Account</button>

                    </form>

                </div>

                <div id="login">
                    <h1>Welcome Back!</h1>

                    <form action="/" method="post">

                        <div class="field-wrap">
                            <input name="" placeholder="Email" type="email" required autocomplete="off" />
                        </div>

                        <div class="field-wrap">
                            <input placeholder="Password" type="password" required autocomplete="off" />
                        </div>

                        <p class="forgot"><a href="{{ route('clients') }}">log In and Register</a></p>

                        <button class="button button-block">Log In</button>

                    </form>

                </div>

            </div><!-- tab-content -->

        </div> <!-- /form -->
    </div>

    <div>
        {{-- <img src="/assets/images/headlineBallsBottom.png" alt=""> --}}
    </div>

    @push('scripts')
        <script>
            // $('.form').find('input, textarea').on('keyup blur focus', function(e) {

            //     var $this = $(this),
            //         label = $this.prev('label');

            //     if (e.type === 'keyup') {
            //         if ($this.val() === '') {
            //             label.removeClass('active highlight');
            //         } else {
            //             label.addClass('active highlight');
            //         }
            //     } else if (e.type === 'blur') {
            //         if ($this.val() === '') {
            //             label.removeClass('active highlight');
            //         } else {
            //             label.removeClass('highlight');
            //         }
            //     } else if (e.type === 'focus') {

            //         if ($this.val() === '') {
            //             label.removeClass('highlight');
            //         } else if ($this.val() !== '') {
            //             label.addClass('highlight');
            //         }
            //     }

            // });

            // $('.tab a').on('click', function(e) {

            //     e.preventDefault();

            //     $(this).parent().addClass('active');
            //     $(this).parent().siblings().removeClass('active');

            //     target = $(this).attr('href');

            //     $('.tab-content > div').not(target).hide();

            //     $(target).fadeIn(600);

            // });

        </script>
    @endpush

@endsection
