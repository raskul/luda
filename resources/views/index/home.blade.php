@extends('layouts.layout')
@section('fullPage')
    @push('styles')
        <style>
            .homeContainer {
                padding-top: 10px;
                padding-bottom: 10px;
                width: 100%;
                font-size: 23px;

                display: grid;
                grid-gap: 30px;
                grid-template: auto 175px / 3fr 2fr;
                grid-template-areas:
                    "text image"
                    "address image"
                ;

            }

            .homeContainer>div {
                border-radius: 60px;
            }

            .homeImage>img {
                border-radius: 60px;
            }

            .homeContainer>div,
            .homeContainer>div>img {
                width: 100%;
                height: 100%
            }

            .homeText {
                grid-area: text;
                text-align: center;
                background-color:#1ab1a9 ;
                color: white;
                padding:30px;
                display: flex;
                justify-content: center;
                flex-direction: column;
                line-height: 1.7;
            }

            .homeAddress {
                grid-area: address;
            }

            .homeImage {
                grid-area: image;
            }

            @media(max-width:800px) {
                .homeContainer {

                    width: 100%;
                    font-size: 18px;

                    display: grid;
                    grid-row-gap: 10px;
                    grid-column-gap: 0px;
                    grid-template: auto auto / auto;
                    grid-template-areas:
                        "text "
                        "address"
                        "image"
                    ;
                }

                .homeContainer>div {
                    border-radius: 0;
                }

                .homeImage>img {
                    border-radius: 0;
                }
                .homeImage {
                    display: none;
                }
            }
        </style>
    @endpush

    <div class="homeContainer">
        <div class="homeText">
            <span>{!! $content !!}</span>
        </div>
        <div class="homeAddress">
            <img src="/assets/images/address_logo.png" alt="">
        </div>
        <div class="homeImage">
            <img src="/assets/images/ludaHomePage.jpg" alt="">
        </div>
    </div>

    <div></div>

@endsection
