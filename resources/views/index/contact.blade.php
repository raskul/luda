@extends('layouts.layout')
@section('fullPage')

@php
    $t = translateAllWordsFromTitle();
@endphp

@push('styles')
    <style>
        /* .titleContent{
            display: flex;

        }
        .titleContent img{
            margin-left: 60px;
        } */
    </style>
@endpush

@include('partials.formStyle')


@push('styles')
    <style>
        .textArea{
            height:130px;
        }
    </style>
@endpush


    {{-- <div class="titleContent"> --}}
    <h2>{{ $title }}</h2>
    {{-- <img src="/assets/images/headlineBalls.png" alt=""> --}}
{{-- </div> --}}
<div>{!! $content !!}</div>

<div style="width: 100%">
    <div class="form">

        <div class="tab-content">
            <div id="signup">

                <form action="{{ route('message.sore') }}" method="post">

                    @csrf

                    <div class="field-wrap">
                        <input name="name" placeholder="{{ $t['name'] }}" type="text" autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <input name="email" placeholder="{{ $t['email'] }}" type="email" required autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <input name="phone" placeholder="{{ $t['phone'] }}" type="text" autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <input name="subject" placeholder="{{ $t['subject'] }}" type="text" required
                            autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <textarea class="textArea" name="content" placeholder="{{ $t['message'] }}" required
                            autocomplete="off" ></textarea>
                    </div>

                    <button type="submit" class="button button-block">{{ $t['send'] }}</button>

                </form>

            </div>
            <div></div>
        </div><!-- tab-content -->
    </div> <!-- /form -->
</div>

<div>
    {{-- <img src="/assets/images/headlineBallsBottom.png" alt=""> --}}
</div>
    
@endsection
