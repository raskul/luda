@extends('layouts.layout')
@section('fullPage')

    @push('styles')
    <style>
        /* .titleContent {
            display: flex;

        }

        .titleContent img {
            margin-left: 60px;
        } */

    </style>
@endpush

@include('partials.formStyle')

{{-- <div class="titleContent"> --}}
    <h2>{{ $title }}</h2>
    {{-- <img src="/assets/images/headlineBalls.png" alt=""> --}}
{{-- </div> --}}
<div>{!! $content !!}</div>

<div style="width: 100%">
    <div class="form">
        
        @php
            $t = translateAllWordsFromTitle();
        @endphp

        <ul class="tab-group">
            <li class="tab active"><a href="#signup">{{ $t['sign_up'] }}</a></li>
            <li class="tab"><a href="#login">{{ $t['log_in'] }}</a></li>
        </ul>

        <div class="tab-content">
            <div id="signup">
                <h1>{{ $t['sign_up_for_free'] }}</h1>

                <form action="{{ url('/register') }}" method="post">

                    @csrf

                    <div class="field-wrap">
                        <input name="name" placeholder="{{ $t['name'] }}" type="text" required autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <input name="email" placeholder="{{ $t['email'] }}" type="email" required autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <input name="password" placeholder="{{ $t['set_a_password'] }}" type="password" required
                            autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <input name="password_confirmation" placeholder="{{ $t['retype_password']  }}" type="password" required
                            autocomplete="off" />
                    </div>

                    <button type="submit" class="button button-block">{{ $t['register'] }}</button>

                </form>

            </div>

            <div id="login">
                <h1>{{ $t['welcome_back'] }}!</h1>

                <form action="/login" method="post">

                    @csrf

                    <div class="field-wrap">
                        <input name="email" placeholder="{{ $t['email'] }}" type="email" required autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <input name="password" placeholder="{{ $t['password'] }}" type="password" required autocomplete="off" />
                    </div>

                    <p class="forgot"><a href="{{ route('forgetPassword') }}">{{ $t['forgot_password'] }}?</a></p>

                    <button class="button button-block" type="submit">{{ $t['log_in'] }}</button>

                </form>

            </div>

        </div><!-- tab-content -->

    </div> <!-- /form -->
</div>

<div>
    {{-- <img src="/assets/images/headlineBallsBottom.png" alt=""> --}}
</div>

@push('scripts')
    <script>

        $('.tab a').on('click', function(e) {

            e.preventDefault();

            $(this).parent().addClass('active');
            $(this).parent().siblings().removeClass('active');

            target = $(this).attr('href');

            $('.tab-content > div').not(target).hide();

            $(target).fadeIn(600);

        });

    </script>
@endpush

@endsection
