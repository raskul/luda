@extends('layouts.layout')
@section('fullPage')

    @push('styles')
        <style>
            .containerTable {
                width: 100%;
                margin-left: auto;
                margin-right: auto;
                padding-left: 0px;
                padding-right: 10px;
            }

            .containerTable a {
                text-decoration: none;
            }

            .containerTable ul {
                margin: 0;
                padding: 0;
            }

            .responsive-table li {
                border-radius: 3px;
                padding: 25px 30px;
                display: flex;
                justify-content: space-between;
                margin-bottom: 25px;
            }

            .responsive-table .table-header {
                background-color: #404040;
                font-size: 14px;
                text-transform: uppercase;
                letter-spacing: 0.03em;
                color: white;
            }

            .responsive-table .table-row {
                background-color: #ffffff;
                box-shadow: 0px 0px 9px 0px rgba(0, 0, 0, 0.1);
            }

            .responsive-table .col-1 {
                flex-basis: 35%;
            }

            .responsive-table .col-2 {
                flex-basis: 35%;
            }

            .responsive-table .col-3 {
                flex-basis: 15%;
            }

            .responsive-table .col-4 {
                flex-basis: 15%;
            }

            @media all and (max-width: 767px) {
                .responsive-table .table-header {
                    display: none;
                }

                .responsive-table li {
                    display: block;
                }

                .responsive-table .col {
                    flex-basis: 100%;
                }

                .responsive-table .col {
                    display: flex;
                    padding: 10px 0;
                }

                .responsive-table .col:before {
                    color: #6C7A89;
                    padding-right: 10px;
                    content: attr(data-label);
                    flex-basis: 50%;
                    text-align: right;
                }
            }

        </style>
    @endpush

    @push('styles')
        <style>
            .button {
                border: 0;
                outline: none;
                border-radius: 0;
                padding: 15px 0;
                /* font-size: 2rem; */
                font-weight: 500;
                text-transform: uppercase;
                letter-spacing: 0.1em;
                background: #1ab1a9;
                color: #ffffff;
                -webkit-transition: all 0.5s ease;
                transition: all 0.5s ease;
                -webkit-appearance: none;
            }

            .button:hover,
            .button:focus {
                background: #15918a;
            }

            .button-block {
                display: block;
                width: 200px;
                margin: auto;
                text-decoration: none;
                text-align: center
            }
            .button-block:link, .button-block:visited, .button-block:hover, .button-block:focus {
                color: #ffffff;
            }

            @media(max-width:500px){
                .button-block {
                    width: 70px;
                }
            }
        </style>
    @endpush
    
    @php
        $t = translateAllWordsFromTitle();
    @endphp
    
    <div class="containerAuth">
        <div>
            {{-- <a class="button button-block" href="{{ route('clients.ticket.new.get') }}">New ticket</a> --}}
            @if (count($tickets) > 0)
                <div class="containerTable">
                    <h2><small>{{ $t['list_of_tickets'] }}:</small></h2>
                    <ul class="responsive-table">
                        <li class="table-header">
                            <div class="col col-1">{{ $t['subject'] }}</div>
                            <div class="col col-2">{{ $t['content'] }}</div>
                            <div class="col col-3">{{ $t['created_at'] }}</div>
                            <div class="col col-4">{{ $t['status'] }}</div>
                        </li>
                        @forelse ($tickets as $ticket)
                            @php
                            $rowBGColor = null;
                            $rowColor = null;
                            $rowFontWeight = 'normal';
                            $status = null;
                            if($ticket->status == 'Unread'){
                            $rowBGColor = '#CCE5FF';
                            $rowColor = '#004085';
                            $rowFontWeight = 'normal';
                            $status = $t['waiting_for_an_answer'];
                            }
                            if($ticket->status == 'Read'){
                            $rowBGColor = '#CCE5FF';
                            $rowColor = '#004085';
                            $rowFontWeight = 'normal';
                            $status = $t['waiting_for_an_answer'];
                            }
                            if($ticket->status == 'Answered'){
                            $rowBGColor = '#D4EDDA';
                            $rowColor = '#155724';
                            $rowFontWeight = 'bold';
                            $status = $t['answered'];
                            }
                            if($ticket->status == 'Finished Successful'){
                            $rowBGColor = '#E2E3E5';
                            $rowColor = '#383d41';
                            $rowFontWeight = 'normal';
                            $status = $t['closed'];
                            }
                            if($ticket->status == 'Finished Unsuccessful'){
                            $rowBGColor = '#E2E3E5';
                            $rowColor = '#383d41';
                            $rowFontWeight = 'normal';
                            $status = $t['closed'];
                            }
                            @endphp

                            <a href="{{ route('clients.ticket.single', $ticket->id) }}">
                                <li class="table-row" style="background-color: {{ $rowBGColor }}; color:{{ $rowColor }};">
                                    <div style=" " class="col col-1" data-label="Subject"><span
                                            style="font-weight:{{ $rowFontWeight }}">{{ $ticket->subject }}</span></div>
                                    <div class="col col-2" data-label="Content">{{ substr($ticket->content, 0, 200) }}</div>
                                    <div style="font-size: 10px" class="col col-3" data-label="Create at">{{ $ticket->created_at }}</div>
                                    <div style=" " class="col col-4" data-label="Status"><span
                                            style="font-weight:{{ $rowFontWeight }}">{{ $status }}</span></div>
                                </li>
                            </a>
                        @empty
                            There is no ticket.
                        @endforelse
                    </ul>
        
                </div>
            @else
                {{ $t['open_your_first_ticket'] }}.
            @endif
        </div>

        @include('dashboard.clientMenu')

    </div>

@endsection
