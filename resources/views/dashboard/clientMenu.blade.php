@push('styles')
    <style>
        .containerAuth {
            display: grid;
            grid-template: auto auto / auto;
            grid-template-areas:
                "menu2"
                "body2"
            ;
        }

        .containerAuth>div:first-child {
            grid-area: body2;
        }

        .containerAuth>div:nth-child(2) {
            grid-area: menu2;
        }

    </style>
@endpush
@push('styles')
    <style>
        .containerA {
            width: 100%;
            /* background: black; */
            /* color: white */
        }

        .containerA a {
            text-decoration: none;
        }

        .textA {
            padding: 20px;
            background-color: black;
            border: 1px solid gray;
            margin: 10px ;
            display: inline-block;
            cursor: pointer;
            float: right;
            color: white;
        }

        .linkA a:link,.linkA a:visited,.linkA a:active,.linkA a:hover{
            color: white;
        }

    </style>
@endpush

<div class="containerA">
    <h1>Welcome {{ Auth::user()->name }}, this is your client area.</h1>
    <div class="linkA">
        <div href="{{ url('/logout') }}" class="textA"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Logout
        </div>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
    <div class="linkA">
        <a href="{{ route('clients.ticket') }}"><div class="textA">Tickets</div></a>
    </div>
    <div class="linkA">
        <a href="{{ route('clients.ticket.new.get') }}"><div class="textA">New Ticket</div></a>
    </div>
    @if(Auth::user()->is_admin)
    <div class="linkA">
        <a href="{{ route('admin') }}"><div class="textA">Admin Panel</div></a>
    </div>
    @endif
</div>
