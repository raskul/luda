@extends('layouts.layout')
@section('fullPage')

    @push('styles')
        <style>
            input,
            textarea {
                display: block;
                width: 100%;
                height: 40px;
                padding: 10px;
                font-weight: bold;
                /* font-size: 20px; */
                border: 0.5px solid #1ab1a9;
                margin: 20px 0;
                outline: none;

            }

            textarea {
                height: 250px;
                /* font-size: 18px; */
            }

        </style>
    @endpush

    @push('styles')
        <style>
            .button {
                border: 0;
                outline: none;
                border-radius: 0;
                padding: 15px 0;
                font-weight: 500;
                text-transform: uppercase;
                letter-spacing: 0.1em;
                background: #1ab1a9;
                color: #ffffff;
                -webkit-transition: all 0.5s ease;
                transition: all 0.5s ease;
                -webkit-appearance: none;
            }

            .button:hover,
            .button:focus {
                background: #15918a;
            }

            .button-block {
                display: block;
                width: 200px;
                /* margin: auto; */
                text-decoration: none;
                text-align: center
            }

            .button-block:link,
            .button-block:visited,
            .button-block:hover,
            .button-block:focus {
                color: #ffffff;
            }

            @media(max-width:500px) {
                .button-block {
                    width: 70px;
                }
            }

        </style>
    @endpush

    @push('styles')
        <style>
            #addFileInput {
                font-size: 11px;
                margin-bottom: 20px;
                border-radius: 20px;
                padding: 10px;
                background-color: #404040;
                color: white;
                display: inline-block;

            }

            #addFileInput:hover {
                cursor: pointer;
                background-color: black;
            }

        </style>
    @endpush

    @php
    $t = translateAllWordsFromTitle();
    @endphp

    <div class="containerAuth">

        <div style="margin-right: 10px;">
            <div style="max-width: 500px; margin: 20px auto 20px auto">
                <form method="POST" action="{{ route('clients.ticket.new.post') }}" enctype="multipart/form-data">

                    @csrf

                    <input id="input" name="subject" placeholder="{{ $t['subject'] }}" required />
                    <textarea id="textarea" name="content" placeholder="{{ $t['message'] }}" required></textarea>

                    <div id="inputContainer">
                        <input type="file" name="file1">
                    </div>

                    <div id="addFileInput">+ {{ $t['add_more_files'] }}</div>

                    <button type="submit" class="button button-block">{{ $t['send'] }}</button>
                </form>
            </div>
        </div>
        @include('dashboard.clientMenu')

    </div>


    @push('scripts')
        <script>
            let addBtn = document.getElementById('addFileInput');
            let inputContainer = document.getElementById('inputContainer');

            let counter = 2;

            addBtn.addEventListener('click', addInput);

            function addInput() {
                let inputElement = document.createElement('input');
                inputElement.name = 'file' + counter;
                inputElement.type = 'file';
                counter++;
                inputContainer.appendChild(inputElement);
                inputElement.click();
            }

        </script>
    @endpush

@endsection
