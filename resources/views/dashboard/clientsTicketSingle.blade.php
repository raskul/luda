@extends('layouts.layout')
@section('fullPage')

    @push('styles')
        <style>
            .containerB {
                margin: 0 auto;
                width: 100%;
                background: #444753;
                border-radius: 5px;
            }

            .containerB ul {
                list-style-type: none;
            }

            .chat {
                width: 100%;
                float: left;
                background: #F2F5F8;
                border-top-right-radius: 5px;
                border-bottom-right-radius: 5px;
                color: #434651;
            }

            .chat .chat-header {
                padding: 20px;
                border-bottom: 2px solid white;
            }

            .chat .chat-header img {
                float: left;
            }

            .chat .chat-header .chat-about {
                float: left;
                padding-left: 10px;
                margin-top: 6px;
            }

            .chat .chat-header .chat-with {
                font-weight: bold;
                font-size: 16px;
            }

            .chat .chat-header .chat-num-messages {
                color: #92959E;
            }

            .chat .chat-header .fa-star {
                float: right;
                color: #D8DADF;
                font-size: 20px;
                margin-top: 12px;
            }

            .chat .chat-history {
                padding: 30px 30px 20px;
                border-bottom: 2px solid white;
                overflow-y: scroll;
                height: 575px;
            }

            .chat .chat-history .message-data {
                margin-bottom: 15px;
            }

            .chat .chat-history .message-data-time {
                color: #a8aab1;
                padding-left: 6px;
            }

            .chat .chat-history .message {
                color: white;
                padding: 18px 20px;
                line-height: 26px;
                font-size: 16px;
                border-radius: 7px;
                margin-bottom: 30px;
                width: 90%;
                position: relative;
            }

            .chat .chat-history .message:after {
                bottom: 100%;
                left: 7%;
                border: solid transparent;
                content: " ";
                height: 0;
                width: 0;
                position: absolute;
                pointer-events: none;
                border-bottom-color: #94C2ED;
                border-width: 10px;
                margin-left: -10px;
            }

            .chat .chat-history .my-message {
                background: #94C2ED;
            }

            .chat .chat-history .other-message {
                background: #86BB71;
            }

            .chat .chat-history .other-message:after {
                border-bottom-color: #86BB71;
                left: 93%;
            }

            .chat .chat-message {
                padding: 30px;
            }

            .chat .chat-message textarea {
                width: 100%;
                border: none;
                padding: 10px 20px;
                font: 14px/22px "Lato", Arial, sans-serif;
                margin-bottom: 10px;
                border-radius: 5px;
                resize: none;
            }

            .chat .chat-message .fa-file-o,
            .chat .chat-message .fa-file-image-o {
                font-size: 16px;
                color: gray;
                cursor: pointer;
            }

            .chat .chat-message button {
                float: right;
                color: #94C2ED;
                font-size: 16px;
                text-transform: uppercase;
                border: none;
                cursor: pointer;
                font-weight: bold;
                background: #F2F5F8;
            }

            .chat .chat-message button:hover {
                color: #75b1e8;
            }

            .align-left {
                text-align: left;
            }

            .align-right {
                text-align: right;
            }

            .float-right {
                float: right;
            }

            .clearfix:after {
                visibility: hidden;
                display: block;
                font-size: 0;
                content: " ";
                clear: both;
                height: 0;
            }

            .message a {
                text-decoration: none;
            }

            .message a:link,
            .message a:visited,
            .message a:active {
                color: black;
                font-size: 11px;
            }

            .message a:hover {}

        </style>
    @endpush

    @push('styles')
        <style>
            #addFileInput {
                font-size: 11px;
                margin-top: 20px;
                border-radius: 20px;
                padding: 6px;
                background-color: #404040;
                color: white;
                display: inline-block;

            }

            #addFileInput:hover {
                cursor: pointer;
                background-color: black;
            }

        </style>
    @endpush

    @php
    $t = translateAllWordsFromTitle();
    @endphp

    <div class="containerAuth">
        <div style="">

            <div class="containerB clearfix">

                <div class="chat">

                    <div class="chat-header clearfix">
                        <div class="chat-about">
                            <div class="chat-with">{{ $t['chat_with'] }} Luna</div>
                        </div>
                        <i class="fa fa-star"></i>
                    </div> <!-- end chat-header -->

                    <div class="chat-history" id="chat-history">
                        <ul>

                            @forelse ($messages as $message)

                                @if ($message->user_id == Auth::user()->id)
                                    <li class="clearfix">
                                        <div class="message-data align-right">
                                            <span class="message-data-time">{{ $message->created_at }}</span> &nbsp; &nbsp;
                                            <span class="message-data-name">{{ $message->user->name }}</span> <i
                                                class="fa fa-circle me"></i>
                                        </div>
                                        <div class="message other-message float-right">

                                            {!! $message->content !!}
                                            {!! printFiles($message->files) !!}

                                        </div>
                                    </li>
                                @else
                                    <li>
                                        <div class="message-data">
                                            <span class="message-data-name"><i class="fa fa-circle online"></i>
                                                {{ $message->user->name }}</span>
                                            <span class="message-data-time">{{ $message->created_at }}</span>
                                        </div>
                                        <div class="message my-message">

                                            {!! $message->content !!}
                                            {!! printFiles($message->files) !!}

                                        </div>
                                    </li>
                                @endif

                            @empty
                                There is no data.
                            @endforelse

                        </ul>

                    </div> <!-- end chat-history -->

                    <div class="chat-message clearfix">

                        <form method="POST" action="{{ route('clients.ticket.answer.post', $messages[0]->id) }}"
                            enctype="multipart/form-data">

                            @csrf

                            <textarea name="content" id="message-to-send" placeholder="Type your message"
                                rows="3"></textarea>

                            <div id="inputContainer">
                                <input type="file" name="file1">
                            </div>

                            <div id="addFileInput" style="">+ {{ $t['add_more_files'] }}</div>

                            <button type="submit">{{ $t['send'] }}</button>
                        </form>
                    </div> <!-- end chat-message -->

                </div> <!-- end chat -->

            </div> <!-- end container -->

            @push('scripts')
                <script>
                    let addBtn = document.getElementById('addFileInput');
                    let inputContainer = document.getElementById('inputContainer');

                    let counter = 2;

                    addBtn.addEventListener('click', addInput);

                    function addInput() {
                        let inputElement = document.createElement('input');
                        inputElement.name = 'file' + counter;
                        inputElement.type = 'file';
                        counter++;

                        console.log(inputElement);

                        inputContainer.appendChild(inputElement);
                    }

                </script>

                <script>
                    let chatHistory = document.getElementById("chat-history");
                    chatHistory.scrollTop = chatHistory.scrollHeight;

                </script>

                <script>
                    this.scrollTo(0, document.documentElement.scrollHeight);

                </script>
            @endpush

        </div>

        @include('dashboard.clientMenu')

    </div>

@endsection
