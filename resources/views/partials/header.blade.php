@php
    $langTemp = Session::get('languages');

    $tempMenu = App\Models\Admin\Setting::where('name', 'menu_home')->orWhere('name', 'menu_about')->orWhere('name', 'menu_clients')->orWhere('name', 'menu_vacancies')->orWhere('name', 'menu_contact')->pluck( 'title_'.$langTemp);
    $homeMenu = $tempMenu[0];
    $aboutMenu = $tempMenu[1];
    $clientsMenu = $tempMenu[2];
    $vacanciesMenu = $tempMenu[3];
    $contactMenu = $tempMenu[4];
@endphp

<header class="header">
    <div class="logo">
        <div>
            <img class="logo-image" src="/assets/images/logo.png" alt="">
        </div>
        <div>
            <span style="font-size: 20px;">language:</span>
            
            <div class="tooltip">
                <a href="/language/dk">
                    <img width="30px" src="/assets/images/dk.jpg" alt="Dansk">
                </a>
                <span class="tooltiptext">Dansk</span>
            </div>
            <div class="tooltip">
                <a href="/language/en">
                    <img width="30px" src="/assets/images/en.png" alt="English">
                </a>
                <span class="tooltiptext">English</span>
            </div>
            <div class="tooltip">
                <a href="/language/ru">
                    <img width="30px" src="/assets/images/ru.jpg" alt="русский">
                </a>
                <span class="tooltiptext">русский</span>
            </div>
            <div class="tooltip">
                <a href="/language/ua">
                    <img width="30px" src="/assets/images/ua.png" alt="українська">
                </a>
                <span class="tooltiptext">українська</span>
            </div>
        </div>
    </div>
    <menu class="menu">
        <div class="mobile-lists" id="mobile-lists">
            <div></div>
            <div></div>
            <div></div>
        </div>
        <ul class="lists" id="lists">
            <li class="list {{ (Route::currentRouteName() == 'index')? 'active' : ''}}"><a class="list-link" href="{{ route('index') }}">{{ $homeMenu }}</a></li>
            <li class="list {{ (Route::currentRouteName() == 'about')? 'active' : ''}}"><a class="list-link" href="{{ route('about') }}">{{ $aboutMenu }}</a></li>
            <li class="list {{ Request::is('clients*') ? 'active' : ''}}"><a class="list-link" href="{{ route('clients') }}">{{ $clientsMenu }}</a></li>
            <li class="list {{ (Route::currentRouteName() == 'vacancies')? 'active' : ''}}"><a class="list-link" href="{{ route('vacancies') }}">{{ $vacanciesMenu }}</a></li>
            <li class="list {{ (Route::currentRouteName() == 'contact')? 'active' : ''}}"><a class="list-link" href="{{ route('contact') }}">{{ $contactMenu }}</a></li>
        </ul>
    </menu>
     
</header>
