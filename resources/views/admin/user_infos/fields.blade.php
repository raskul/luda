<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Next Contact Day Field -->
<div class="form-group col-sm-6">
    {!! Form::label('next_contact_day', 'Next Contact Day:') !!}
    {!! Form::text('next_contact_day', null, ['class' => 'form-control','id'=>'next_contact_day']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#next_contact_day').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Next Contact Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('next_contact_note', 'Next Contact Note:') !!}
    {!! Form::textarea('next_contact_note', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone2', 'Phone2:') !!}
    {!! Form::text('phone2', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone3', 'Phone3:') !!}
    {!! Form::text('phone3', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone4 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone4', 'Phone4:') !!}
    {!! Form::text('phone4', null, ['class' => 'form-control']) !!}
</div>

<!-- Rule Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rule', 'Rule:') !!}
    {!! Form::text('rule', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.userInfos.index') }}" class="btn btn-default">Cancel</a>
</div>
