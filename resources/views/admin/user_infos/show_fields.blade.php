<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $userInfo->id }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $userInfo->user_id }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $userInfo->description }}</p>
</div>

<!-- Next Contact Day Field -->
<div class="form-group">
    {!! Form::label('next_contact_day', 'Next Contact Day:') !!}
    <p>{{ $userInfo->next_contact_day }}</p>
</div>

<!-- Next Contact Note Field -->
<div class="form-group">
    {!! Form::label('next_contact_note', 'Next Contact Note:') !!}
    <p>{{ $userInfo->next_contact_note }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $userInfo->phone }}</p>
</div>

<!-- Phone2 Field -->
<div class="form-group">
    {!! Form::label('phone2', 'Phone2:') !!}
    <p>{{ $userInfo->phone2 }}</p>
</div>

<!-- Phone3 Field -->
<div class="form-group">
    {!! Form::label('phone3', 'Phone3:') !!}
    <p>{{ $userInfo->phone3 }}</p>
</div>

<!-- Phone4 Field -->
<div class="form-group">
    {!! Form::label('phone4', 'Phone4:') !!}
    <p>{{ $userInfo->phone4 }}</p>
</div>

<!-- Rule Field -->
<div class="form-group">
    {!! Form::label('rule', 'Rule:') !!}
    <p>{{ $userInfo->rule }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $userInfo->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $userInfo->updated_at }}</p>
</div>

