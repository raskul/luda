@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Messages</h1>
        <h1 class="pull-right">
            {{-- <a class="btn btn-primary pull-right"
                style="margin-top: -10px;margin-bottom: 5px" href="{{ route('admin.messages.create') }}">Add New</a>
            --}}
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <table class="table table-striped table-inverse table-responsive">
                    <thead class="thead-inverse">
                        <tr>
                            <th style="width: 200px">Action</th>
                            <th style="width: 100px">Reply</th>
                            <th style="width: 50px">ID</th>
                            <th style="width: 250px">Content</th>
                            <th style="width: 200px">Email</th>
                            <th style="width: 100px">phone</th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($tickets as $ticket)
                            @php
                            $rowBGColor = null;
                            $rowFontWeight = 'normal';
                            if($ticket->status == 'Unread'){
                            $rowBGColor = '#F8D7DA';
                            $rowFontWeight = 'bold';
                            }
                            if($ticket->status == 'Read'){
                            $rowBGColor = '#FFF3CD';
                            $rowFontWeight = 'bold';
                            }
                            if($ticket->status == 'Answered'){
                            $rowBGColor = '#D4EDDA';
                            $rowFontWeight = 'normal';
                            }
                            if($ticket->status == 'Finished Successful'){
                            $rowBGColor = '#E2E3E5';
                            $rowFontWeight = 'normal';
                            }
                            if($ticket->status == 'Finished Unsuccessful'){
                            $rowBGColor = '#E2E3E5';
                            $rowFontWeight = 'normal';
                            }
                            @endphp
                            <tr style="background-color: {{ $rowBGColor }};">
                                <td style="font-weight:{{ $rowFontWeight }}" scope="row">
                                    <form action="{{ route('admin.message.changeStatus', $ticket->id) }}" method="post">
                                        @csrf
                                        <Select name="status" class="form-control" style="width:120px; display: inline-block">
                                            <option value="Unread" {{ $ticket->status == 'Unread' ? 'selected' : '' }}>
                                                Unread</option>
                                            <option value="Read" {{ $ticket->status == 'Read' ? 'selected' : '' }}>
                                                Read</option>
                                            <option value="Answered" {{ $ticket->status == 'Answered' ? 'selected' : '' }}>
                                                Answered</option>
                                            <option value="Finished Successful"
                                                {{ $ticket->status == 'Finished Successful' ? 'selected' : '' }}>Finished
                                                Successful</option>
                                            <option value="Finished Unsuccessful"
                                                {{ $ticket->status == 'Finished Unsuccessful' ? 'selected' : '' }}>Finished
                                                Unsuccessful</option>
                                        </Select>
                                        <button class="btn btn-primary" type="submit">submit</button>
                                    </form>
                                </td>
                                <td style="font-weight:{{ $rowFontWeight }}">
                                    <a href="{{ route('admin.message.reply.get', $ticket->id) }}" class="btn btn-success">
                                        Reply

                                        @if ($ticket->user_id == null)
                                            with email
                                        @endif

                                    </a>
                                    @if ($ticket->user_id == null)
                                        <div style="font-size: 9px;color: gray;">Your reply will send to Email.
                                            <br>This user is not registered.</div>
                                    @endif
                                </td>
                                <td style="font-weight:{{ $rowFontWeight }}">{{ $ticket->id }}</td>

                                @php
                                $content = "";
                                if(strlen($ticket->subject) > 0){
                                $content = $ticket->subject;
                                }else if(strlen($ticket->content) > 0 ){
                                $content = substr($ticket->content, 0, 200);
                                }else {
                                $content = substr($ticket->files, 0, 200);
                                }
                                @endphp

                                <td style="font-weight:{{ $rowFontWeight }}">{{ $content }}</td>
                                <td style="font-weight:{{ $rowFontWeight }}">{{ $ticket->user_email }}</td>
                                <td style="font-weight:{{ $rowFontWeight }}">{{ $ticket->phone }}</td>

                            </tr>
                        @empty
                            There is no data.
                        @endforelse

                    </tbody>
                </table>
                {!! $tickets->links() !!}
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
