<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Verified At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_admin', 'Is admin:') !!}
    <select name="is_admin" class="form-control" id="">
        <option value="0" @if(isset($user)) {{ $user->is_admin?'':'selected' }} @endif>client</option>
        <option value="1" @if(isset($user)) {{ $user->is_admin?'selected':'' }} @endif>Administrator</option>
    </select>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    <input name="description" value="{{ isset($user) ? $user->user_info->description : ''}}" type="text" class="form-control">
</div>

<div class="form-group col-sm-6">
    {!! Form::label('next_contact_note', 'Next contact note:') !!}
    <input name="next_contact_note" value="{{ isset($user) ? $user->user_info->next_contact_note : ''}}" type="text" class="form-control">
</div>

<div class="form-group col-sm-6">
    {!! Form::label('next_contact_day', 'Next contact date:') !!}
    <input name="next_contact_day" value="{{ isset($user) ? substr($user->user_info->next_contact_day,0 ,10) : ''}}" type="date" class="form-control">
</div>

<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone1:') !!}
    <input name="phone" value="{{ isset($user) ? $user->user_info->phone : ''}}" type="text" class="form-control">
</div>
<div class="form-group col-sm-6">
    {!! Form::label('phone2', 'Phone2:') !!}
    <input name="phone2" value="{{ isset($user) ? $user->user_info->phone2 : ''}}" type="text" class="form-control">
</div>
<div class="form-group col-sm-6">
    {!! Form::label('phone3', 'Phone3:') !!}
    <input name="phone3" value="{{ isset($user) ? $user->user_info->phone3 : ''}}" type="text" class="form-control">
</div>
<div class="form-group col-sm-6">
    {!! Form::label('phone4', 'Phone4:') !!}
    <input name="phone4" value="{{ isset($user) ? $user->user_info->phone4 : ''}}" type="text" class="form-control">
</div>

<div class="form-group col-sm-6">
    {!! Form::label('rule', 'rule:') !!}
    <input name="rule" value="{{ isset($user) ? $user->user_info->rule : ''}}" type="text" class="form-control">
</div>

@push('scripts')
    <script type="text/javascript">
        $('#email_verified_at').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.users.index') }}" class="btn btn-default">Cancel</a>
</div>
