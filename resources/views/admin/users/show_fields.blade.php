<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $user->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $user->name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $user->email }}</p>
</div>

</div>

<!-- Is Admin Field -->
<div class="form-group">
    {!! Form::label('is_admin', 'Is Admin:') !!}
    <p>{{ $user->is_admin ? 'admin' : 'client' }}</p>
</div>

<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $user->user_info->description !!}</p>
</div>

<div class="form-group">
    {!! Form::label('next_contact_note', 'Next contact note:') !!}
    <p>{!! $user->user_info->next_contact_note !!}</p>
</div>

<div class="form-group">
    {!! Form::label('next_contact_day', 'Next contact date:') !!}
    <p>{!! $user->user_info->next_contact_day !!}</p>
</div>

<div class="form-group">
    {!! Form::label('phone', 'Phone1:') !!}
    <p>{!! $user->user_info->phone !!}</p>
</div>
<div class="form-group">
    {!! Form::label('phone', 'Phone2:') !!}
    <p>{!! $user->user_info->phone2 !!}</p>
</div>
<div class="form-group">
    {!! Form::label('phone', 'Phone3:') !!}
    <p>{!! $user->user_info->phone3 !!}</p>
</div>
<div class="form-group">
    {!! Form::label('phone', 'Phone4:') !!}
    <p>{!! $user->user_info->phone4 !!}</p>
</div>
<div class="form-group">
    {!! Form::label('rule', 'rule:') !!}
    <p>{!! $user->user_info->rule !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $user->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $user->updated_at }}</p>
</div>

