<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control invisible' ]) !!}
</div>

<!-- Title En Field -->
@if ($setting['title_en'])
    <div class="form-group col-sm-6">
        {!! Form::label('title_en', 'Title En:') !!}
        {!! Form::text('title_en', null, ['class' => 'form-control']) !!}
    </div>
@endif

<!-- Content En Field -->
@if ($setting['content_en'])
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('content_en', 'Content En:') !!}
        {!! Form::textarea('content_en', null, ['class' => 'form-control']) !!}
    </div>
@endif

<!-- Title Dk Field -->
@if ($setting['title_dk'])
<div class="form-group col-sm-6">
    {!! Form::label('title_dk', 'Title Dk:') !!}
    {!! Form::text('title_dk', null, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Content Dk Field -->
@if ($setting['content_dk'])
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content_dk', 'Content Dk:') !!}
    {!! Form::textarea('content_dk', null, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Title Ru Field -->
@if ($setting['title_ru'])
<div class="form-group col-sm-6">
    {!! Form::label('title_ru', 'Title Ru:') !!}
    {!! Form::text('title_ru', null, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Content Ru Field -->
@if ($setting['content_ru'])
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content_ru', 'Content Ru:') !!}
    {!! Form::textarea('content_ru', null, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Title Ua Field -->
@if ($setting['title_ua'])
<div class="form-group col-sm-6">
    {!! Form::label('title_ua', 'Title Ua:') !!}
    {!! Form::text('title_ua', null, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Content Ua Field -->
@if ($setting['content_ua'])
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content_ua', 'Content Ua:') !!}
    {!! Form::textarea('content_ua', null, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.settings.index') }}" class="btn btn-default">Cancel</a>
</div>


@include('partials.tinymcefull')
