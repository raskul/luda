<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $setting->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $setting->name }}</p>
</div>

<!-- Title En Field -->
<div class="form-group">
    {!! Form::label('title_en', 'Title En:') !!}
    <p>{{ $setting->title_en }}</p>
</div>

<!-- Content En Field -->
<div class="form-group">
    {!! Form::label('content_en', 'Content En:') !!}
    <p>{{ $setting->content_en }}</p>
</div>

<!-- Title Dk Field -->
<div class="form-group">
    {!! Form::label('title_dk', 'Title Dk:') !!}
    <p>{{ $setting->title_dk }}</p>
</div>

<!-- Content Dk Field -->
<div class="form-group">
    {!! Form::label('content_dk', 'Content Dk:') !!}
    <p>{{ $setting->content_dk }}</p>
</div>

<!-- Title Ru Field -->
<div class="form-group">
    {!! Form::label('title_ru', 'Title Ru:') !!}
    <p>{{ $setting->title_ru }}</p>
</div>

<!-- Content Ru Field -->
<div class="form-group">
    {!! Form::label('content_ru', 'Content Ru:') !!}
    <p>{{ $setting->content_ru }}</p>
</div>

<!-- Title Ua Field -->
<div class="form-group">
    {!! Form::label('title_ua', 'Title Ua:') !!}
    <p>{{ $setting->title_ua }}</p>
</div>

<!-- Content Ua Field -->
<div class="form-group">
    {!! Form::label('content_ua', 'Content Ua:') !!}
    <p>{{ $setting->content_ua }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $setting->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $setting->updated_at }}</p>
</div>

