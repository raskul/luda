<div class="table-responsive">
    <table class="table" id="settings-table">
        <thead>
            <tr>
                <th>Action</th>
                <th>Id</th>
                <th>Name</th>
                <th>Title En</th>
                <th>Content En</th>
                <th>Title Dk</th>
                <th>Content Dk</th>
                <th>Title Ru</th>
                <th>Content Ru</th>
                <th>Title Ua</th>
                <th>Content Ua</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($settings as $setting)
                <tr>
                    <td>
                        {!! Form::open(['route' => ['admin.settings.destroy', $setting->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('admin.settings.show', [$setting->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{{ route('admin.settings.edit', [$setting->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                            {{-- {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class'
                            => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!} --}}
                        </div>
                        {!! Form::close() !!}
                    </td>
                    <td>{{ $setting->id }}</td>
                    <td>{{ $setting->name }}</td>
                    <td>{{ substr($setting->title_en , 0 , 100) }}</td>
                    <td>{{ substr($setting->content_en , 0 , 100) }}</td>
                    <td>{{ substr($setting->title_dk , 0 , 100) }}</td>
                    <td>{{ substr($setting->content_dk , 0 , 100) }}</td>
                    <td>{{ substr($setting->title_ru , 0 , 100) }}</td>
                    <td>{{ substr($setting->content_ru , 0 , 100) }}</td>
                    <td>{{ substr($setting->title_ua , 0 , 100) }}</td>
                    <td>{{ substr($setting->content_ua , 0 , 100) }}</td>
                    
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
