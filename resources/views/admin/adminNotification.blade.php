@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Notifications Settings</h1>
        {{-- <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
                href="{{ route('admin.messages.create') }}">Add New</a>
        </h1> --}}
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                @include('partials.messages')

                <form action="{{ route('notificationsShowFormPost') }}" method="POST">

                    @csrf

                    <div class="form-group">
                        <label for="">Admin Email To Get Notification</label>
                        <input type="text" class="form-control" name="admin_email" id="" aria-describedby="helpId"
                            placeholder="example@example.com" value="{{ $admin_email }}">
                        <small id="helpId" class="form-text text-muted">Notifications will send to this email
                            address</small>
                    </div>
                    
                    <div class="" style="background-color: rgb(253, 231, 231) ; border:1px red solid ;padding: 20px">
                        <h3>Your G-mail information:</h3>
                        <div class="form-group">
                            <label for="">Email To Config Send Notification</label>
                            <input type="text" class="form-control" name="admin_send_email" id="" aria-describedby="helpId"
                                placeholder="example@example.com" value="{{ $admin_send_email }}">
                            <small id="helpId" class="form-text text-muted">Enter your G-mail to send email to other users.</small>
                        </div>

                        <div class="form-group">
                            <label for="">Enter the password of G-mail to config send email notification</label>
                            <input type="text" class="form-control" name="admin_send_email_password" id=""
                                aria-describedby="helpId" placeholder="example@example.com" value="********">
                            <small id="helpId" class="form-text text-muted">The password of your G-mail to send email to
                                others.
                                just first time enter your password here and don't change it, if your password is not changed.</small>
                        </div>

                        <div>
                            <h3>Your G-mail settings:</h3>
                            <h4>How to allow access for less secure app to interact with gmail account. use smtp server to
                                send
                                mail</h4>
                            <p style="font-size: small">1- YOU HAVE TO ENABLE LESS SECURE APP. Less secure apps, you may refer to details here:
                                <a
                                    href="https://support.google.com/accounts/answer/6010255?hl=en">https://support.google.com/accounts/answer/6010255?hl=en</a>.
                                <br>2- YOU HAVE TO ENABLE 2-STEP VERIFICATION. If you have 2-step verification
                                enabled on that account, you won't see the option to "allow for less secure apps" and would
                                need
                                to use an application-specific password instead:
                                <a
                                    href="https://support.google.com/accounts/answer/185833?hl=en">https://support.google.com/accounts/answer/185833?hl=en</a>.
                            </p>
                        </div>
                    </div>
                    <br>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" @if ($new_user_register == 'yes') checked @endif
                            name="new_user_register" id="" value="yes" >
                            Notify admin if new user registered.
                        </label>
                    </div>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" @if ($user_send_message_to_admin == 'yes') checked @endif
                            name="user_send_message_to_admin" id="" value="yes" >
                            Notify admin if user send message.
                        </label>
                    </div>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" @if ($admin_answer_user_message == 'yes') checked @endif
                            name="admin_answer_user_message" id="" value="yes" >
                            Notify users when admin, answered their messages.
                        </label>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary">Save Changes</button>
                    </div>

                </form>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
