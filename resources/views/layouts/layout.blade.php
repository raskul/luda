<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/assets/style.css">
    <link rel="stylesheet" href="/assets/mobile-style.css">
    <title>

        @yield('title', 'Luna lawyer')

    </title>

    @stack('styles')

</head>

<body>

    <div class="container">

        @include('partials.header')

        <section class="main">
            <div class="main-content">

                @include('partials.messages')
     
                @hasSection ('fullPage')

                    @yield('fullPage')

                @else

                    @yield('content')

                    </div>
                    <div class="right-logo">
                    <img src="/assets/images/right-logo.png" alt="">

                @endif

            
            </div>
        </section>

        @include('partials.footer')

    </div>
    <script src="/assets/script.js"></script>

    @stack('scripts')

</body>

</html>
