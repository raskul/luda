<li style="border-bottom: gray 1px solid">
    <a href="{{ route('index') }}"><i class="fa fa-tv"></i><span>Website</span></a>
</li>

<li class="{{ Request::is('admin') ? 'active' : '' }}">
    <a href="{{ route('admin') }}"><i class="fa fa-home"></i><span>Admin</span></a>
</li>

<li class="{{ Request::is('admin/message') ? 'active' : '' }}">
    <a href="{{ route('admin.message') }}"><i class="fa fa-envelope"></i><span>Messages</span></a>
</li>

<li class="{{ Request::is('admin/users*') ? 'active' : '' }}">
    <a href="{{ route('admin.users.index') }}"><i class="fa fa-user"></i><span>Users</span></a>
</li>
<li class="{{ Request::is('admin/notifications*') ? 'active' : '' }}">
    <a href="{{ route('notificationsShowForm') }}"><i class="fa fa-bell-o"></i><span>Notifications</span></a>
</li>

<li class="{{ Request::is('admin/messages*') ? 'active' : '' }}" style="border-top: 1px gray solid" >
    <a href="{{ route('admin.messages.index') }}"><i class="fa fa-envelope-open"></i><span>Advance Messages</span></a>
</li>

<li class="{{ Request::is('admin/settings*') ? 'active' : '' }}">
    <a href="{{ route('admin.settings.index') }}"><i class="fa fa-gear"></i><span>Advance Settings</span></a>
</li>

{{-- <li class="{{ Request::is('admin/userInfos*') ? 'active' : '' }}">
    <a href="{{ route('admin.userInfos.index') }}"><i class="fa fa-edit"></i><span>User Infos</span></a>
</li> --}}



