<?php

return array (
  'singular' => 'Message',
  'plural' => 'Messages',
  'fields' => 
  array (
    'id' => 'Id',
    'user_id' => 'User Id',
    'user_email' => 'User Email',
    'parent_id' => 'Parent Id',
    'name' => 'Name',
    'subject' => 'Subject',
    'content' => 'Content',
    'files' => 'Files',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
