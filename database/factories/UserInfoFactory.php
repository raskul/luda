<?php

namespace Database\Factories\Admin;

use App\Models\Admin\UserInfo;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserInfoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserInfo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->word,
        'description' => $this->faker->word,
        'next_contact_day' => $this->faker->word,
        'next_contact_note' => $this->faker->word,
        'phone' => $this->faker->word,
        'phone2' => $this->faker->word,
        'phone3' => $this->faker->word,
        'phone4' => $this->faker->word,
        'rule' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
