<?php

namespace Database\Factories\Admin;

use App\Models\Admin\Message;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'user_id' => $this->faker->word,
        'user_email' => $this->faker->word,
        'parent_id' => $this->faker->word,
        'name' => $this->faker->word,
        'phone' => $this->faker->word,
        'subject' => $this->faker->word,
        'content' => $this->faker->word,
        'files' => $this->faker->word,
        'status' => $this->faker->randomElement(),
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
