<?php

namespace Database\Seeders;

use App\Models\Admin\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //start fill menu
        Setting::insert([
            'name' => 'menu_home',
            'title_en' => 'Home',
            'title_dk' => 'Hjem',
            'title_ru' => 'ДОМ',
            'title_ua' => 'ДОМА',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'menu_about',
            'title_en' => 'About us',
            'title_dk' => 'om os',
            'title_ru' => 'насчет нас',
            'title_ua' => 'про нас',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'menu_clients',
            'title_en' => 'Clients',
            'title_dk' => 'kunder',
            'title_ru' => 'клиенты',
            'title_ua' => 'клієнтів',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'menu_vacancies',
            'title_en' => 'vacancies',
            'title_dk' => 'ledige stillinger',
            'title_ru' => 'вакансии',
            'title_ua' => 'вакансії',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'menu_contact',
            'title_en' => 'contact',
            'title_dk' => 'kontakt',
            'title_ru' => 'контакт',
            'title_ua' => 'контакт',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        //end fill menu

        //start contexts
        Setting::insert([
            'name'=>'home_text',
            'title_en' => null,
            'title_dk' => null,
            'title_ru' => null,
            'title_ua' => null,
            'content_en' => 'Litigation and advice <br> at the highest level within <br> personal and property taxation',
            'content_dk' => 'Sagsførelse og rådgivning<br>på højeste niveau indenfor<br>person- og ejendomsbeskatning',
            'content_ru' => 'Судебные разбирательства и консультации<br>на самом высоком уровне внутри<br>налогообложение физических лиц и имущества',
            'content_ua' => 'Судова тяганина та поради<br> на найвищому рівні всередині<br> оподаткування особистого та майна',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'about_us_text',
            'title_en' => 'About us',
            'title_dk' => 'Om os',
            'title_ru' => 'О нас',
            'title_ua' => 'Про нас',
            'content_en' => 'Milla is a new law firm whose immediate focal point is a large portfolio of cases within the tax area. We have joined forces with the aim of utilizing our competencies to provide tax advice at the very highest level.Milla\'s extensive portfolio and know-how means that we possess expertise that makes us pioneers in a number of tax law retail areas with the opportunity to change and develop both administrative practice and case law."Our vision is to be a major player in parts of tax law.We want to be the natural choice for clients with issueswithin our core competencies. "Milla has a strong commitment to and respect for the legal profession itself, and the legal as well as ethical rules associated with it. It is therefore our goal to practice the profession of lawyer in a professional way, with great regard for both quality and integrity in relation to the people around us.Milla\'s basic attitudes are a high level of ambition, innovation and willingness to change. Milla\'s success depends on our basic attitudes being followed. We therefore work purposefully to develop in accordance with our goals and visions. We believe that it is the human resources that create the result. Therefore, we emphasize that for the individual employee there must be room for both work, family and leisure.',
            'content_dk' => 'Milla er et nyt advokatfirma, hvis umiddelbare omdrejningspunkt er en stor portefølje af sager inden for skatteområdet. Vi har slået os sammen med det formål at udnytte vores kompetencer til at yde skatterådgivning på det højeste niveau. Millas omfattende portefølje og know-how betyder, at vi besidder ekspertise, der gør os til pionerer inden for en række skatteretlige detailområder med mulighed for at ændre og udvikle både administrativ praksis og retspraksis. "Vores vision er at være en vigtig aktør inden for dele af skatteretten. Vi vil være det naturlige valg for kunder med problemer inden for vores kernekompetencer." Milla har et stærkt engagement i og respekt for selve advokatyrket og de juridiske såvel som etiske regler, der er knyttet til det. Det er derfor vores mål at udøve advokatyrket på en professionel måde med stor respekt for både kvalitet og integritet i forhold til menneskerne omkring os.Millas grundlæggende holdninger er et højt niveau af ambition, innovation og vilje til at ændre . Millas succes afhænger af, at vores grundlæggende holdninger følges. Vi arbejder derfor målrettet med at udvikle os i overensstemmelse med vores mål og visioner. Vi mener, at det er de menneskelige ressourcer, der skaber resultatet. Derfor understreger vi, at der for den enkelte medarbejder skal være plads til både arbejde, familie og fritid.',
            'content_ru' => 'Milla - это новая юридическая фирма, непосредственным фокусом которой является большой портфель дел в налоговой сфере. Мы объединили свои усилия с целью использования нашей компетенции для предоставления налоговых консультаций на самом высоком уровне. Обширный портфель и ноу-хау Milla означают, что мы обладаем опытом, который делает нас пионерами в ряде областей розничного налогового права с возможность изменять и развивать как административную практику, так и прецедентное право. «Наше видение состоит в том, чтобы стать крупным игроком в некоторых областях налогового права. Мы хотим быть естественным выбором для клиентов, у которых есть проблемы в рамках нашей основной компетенции». Milla твердо привержена и уважение к самой профессии юриста, а также к правовым и этическим нормам, связанным с ней. Поэтому наша цель - профессионально заниматься профессией юриста, уделяя большое внимание качеству и порядочности по отношению к окружающим нас людям. Основное отношение Milla - это высокие амбиции, новаторство и готовность к изменениям. . Успех Milla зависит от наших основных взглядов. Поэтому мы целенаправленно работаем, чтобы развиваться в соответствии с нашими целями и видениями. Мы считаем, что результат создают человеческие ресурсы. Поэтому мы подчеркиваем, что для отдельного сотрудника должно быть место как для работы, так и для семьи и отдыха.',
            'content_ua' => 'Milla - нова юридична фірма, безпосереднім центром якої є великий портфель справ у податковій сфері. Ми об\єднали зусилля з метою використання наших компетенцій для надання податкових консультацій на найвищому рівні. Широкий портфель та ноу-хау Milla означає, що ми маємо досвід, що робить нас першопрохідцями в ряді податкових правових роздрібних галузей з можливість змінити та розвинути як адміністративну практику, так і прецедентне право. "Наше бачення полягає у тому, щоб бути основним гравцем у частинах податкового законодавства. Ми хочемо бути природним вибором для клієнтів, що мають проблеми, що належать до наших основних компетенцій". Milla твердо прагне до та повага самої адвокатської професії, а також юридичних, а також етичних норм, пов\язаних з нею. Тому наша мета - професійно займатися адвокатською професією з великим повагою як до якості, так і до доброчесності щодо людей, які нас оточують. Основними установками Milla є високий рівень амбіцій, інновацій та готовність до змін. . Успіх Milla залежить від наших основних поглядів. Тому ми цілеспрямовано працюємо над розвитком відповідно до наших цілей та бачень. Ми віримо, що саме людські ресурси дають результат. Тому ми наголошуємо, що для окремого працівника має бути місце як для роботи, так і для сім’ї та відпочинку.',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'clients_text',
            'title_en' => 'Clients',
            'title_dk' => 'Klienter',
            'title_ru' => 'Клиенты',
            'title_ua' => 'Клієнти',
            'content_en' => 'Milla has a wide range of clients, which we currently serve with our knowledge of tax law. The portfolio includes private individuals, cooperative housing associations, business companies, administration companies, insurance companies, construction companies and non-profit organizations. We are aware that our clients have a busy everyday life and therefore offer that you can always follow the status of the case online. We also make sure to update the system every time there is a development in a specific case. As a customer, you will automatically be offered a customer login. Should this unexpectedly not have happened, please send an email to Milla@Milla.dk Milla\'s terms and conditions',
            'content_dk' => 'Milla har en bred vifte af klienter, som vi i dag betjener med vores viden indenfor skatteret. Porteføljen indeholder privatpersoner, andelsboligforeninger, erhvervsvirksomheder, administrationsselskaber, forsikringsselskaber, entreprenørvirksomheder samt almennyttige organisationer.Vi er bekendt med at vores klienter har en travl hverdag og tilbyder derfor, at man altid kan følge med i sagens status online. Vi sørger ligeledes for, at opdatere systemet hver gang der sker en udvikling i en konkret sag.Som kunde vil du automatisk få tilbudt et kundelogin. Skulle dette mod forventning ikke være sket, bedes du sende en mail til Milla@Milla.dk Millas forretningsbetingelser',
            'content_ru' => 'Milla имеет широкий круг клиентов, которых мы в настоящее время обслуживаем, хорошо разбираясь в налоговом законодательстве. В портфель входят частные лица, кооперативные жилищные ассоциации, коммерческие предприятия, административные компании, страховые компании, строительные компании и некоммерческие организации.Мы осознаем, что у наших клиентов напряженная повседневная жизнь, и поэтому предлагаем вам всегда следить за состоянием дела в Интернете. Мы также не забываем обновлять систему каждый раз, когда происходит разработка в конкретном случае. Как клиенту, вам автоматически будет предложен логин. Если этого не произошло неожиданно, отправьте электронное письмо по адресу Milla@Milla.dk Условия использования Milla.',
            'content_ua' => 'Milla має широке коло клієнтів, яких ми зараз обслуговуємо, знаючи податкове законодавство. Портфоліо включає приватних осіб, кооперативні житлові асоціації, бізнес-компанії, адміністративні компанії, страхові компанії, будівельні компанії та некомерційні організації. Ми усвідомлюємо, що наші клієнти мають напружене повсякденне життя, і тому пропонуємо вам завжди можна відстежувати стан справи в Інтернеті. Ми також подбаємо про те, щоб оновлювати систему щоразу, коли відбувається розробка у конкретному випадку. Як клієнту вам автоматично буде запропоновано вхід для клієнта. Якщо цього несподівано не сталося, надішліть електронне повідомлення на Milla@Milla.dk Умови використання Milla',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'vacancies_text',
            'title_en' => 'Vacancies',
            'title_dk' => 'Ledige stillinger ',
            'title_ru' => 'Вакансии',
            'title_ua' => 'Вакансії',
            'content_en' => 'For Adduco, it is crucial that an everyday life is full of challenges, "speed across the field" and takes place in a framework that is comfortable and motivating for our employees. Therefore, we take pride in being a workplace where there are high ceilings, where there is room for diversity, where the physical as well as mental work environment is top notch. This means that keywords such as sociability, empathy, respect and helpfulness are absolutely crucial in our assessment of whether an applicant is interesting in relation to employment. Currently vacancies There are p.t. no vacancies. Unsolicited applicationsAt Adduco, we can always use people who, in addition to having the right human qualifications, can contribute with professional knowledge and skill. If you are interested in new challenges and can see yourself as part of our company profile, then you are very welcome to send an unsolicited application.',
            'content_dk' => 'For Adduco er det afgørende, at en hverdag er fyldt med udfordringer, ”fart over feltet” og finder sted i rammer, der er behagelige og motiverende for vores medarbejdere. Derfor sætter vi en ære i at være en arbejdsplads, hvor der er højt til loftet, hvor der er plads til forskellighed, hvor det fysiske såvel som psykiske arbejdsmiljø er helt i top. Det betyder, at nøgleord som omgængelighed, empati, respekt og hjælpsomhed, er helt afgørende i vores vurdering af, om en ansøger er interessant i forhold til ansættelse. Aktuelt ledige stillinger Der er p.t. ingen ledige stillinger. Uopfordrede ansøgningerHos Adduco kan vi altid bruge folk der, udover at have de rette menneskelige kvalifikationer, kan bidrage med faglige kundskaber og dygtighed. Er du interesseret i nye udfordringer og kan du se dig selv som en del af vores virksomhedsprofil, så er du meget velkommen til at sende en uopfordret ansøgning.',
            'content_ru' => 'Для Adduco очень важно, чтобы повседневная жизнь была полна проблем, «скорости через поле» и протекала в удобной и мотивирующей среде для наших сотрудников. Поэтому мы гордимся тем, что являемся рабочим местом с высокими потолками, где есть место для разнообразия, где физическая и умственная рабочая среда находится на высшем уровне. Это означает, что такие ключевые слова, как общительность, сочувствие, уважение и услужливость, имеют решающее значение для нашей оценки того, интересен ли кандидат в отношении работы. В настоящее время вакансии Есть п.т. нет вакансий. Незапрошенные приложенияВ Adduco мы всегда можем использовать людей, которые, помимо необходимой квалификации, могут внести свой вклад в виде профессиональных знаний и навыков. Если вас интересуют новые задачи и вы видите себя частью профиля нашей компании, вы можете отправить незапрашиваемое заявление.',
            'content_ua' => 'Для Adduco надзвичайно важливо, щоб повсякденне життя було насиченим викликами, "швидкістю по полю" і проходило в рамках, зручних та спонукальних для наших співробітників. Тому ми пишаємось тим, що є робочим місцем, де є високі стелі, де є місце для різноманітності, де фізичне та розумове робоче середовище є першокласними. Це означає, що такі ключові слова, як комунікабельність, емпатія, повага та корисність, є надзвичайно важливими для нашої оцінки того, чи є заявник цікавим стосовно роботи. В даний час вакансій Є п.т. відсутність вакансій. Незапрошені програмиВ Adduco ми завжди можемо використовувати людей, які, крім того, що мають відповідну кваліфікацію людини, можуть сприяти професійними знаннями та вмінням. Якщо вас цікавлять нові виклики, і ви можете бачити себе частиною профілю нашої компанії, тоді ми дуже раді надіслати небажану заявку.',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'contact_text',
            'title_en' => 'Contact',
            'title_dk' => 'Kontakt',
            'title_ru' => 'Связаться с нами',
            'title_ua' => 'Зв\'язок',
            'content_en' => 'Advokatanpartsselskabet Adduco Phone 46 92 47 99 Email adduco@adduco.dk Cvrnr. 31483891 Branch Randers Lupinvej 10 8920 Randers NV Branch Dubai Jsl sss Free Zone Dubai United Arab Emirates',
            'content_dk' => 'Advokatanpartsselskabet Adduco Telefon	46 92 47 99 Email	adduco@adduco.dkCvrnr.	31483891 Afdeling Randers Lupinvej 10 8920 Randers NV Afdeling Dubai Jsl sss Free Zone Dubai Forenede Arabiske Emirater',
            'content_ru' => 'Advokatanpartsselskabet Adduco Телефон 46 92 47 99 Электронная почта adduco@adduco.dk Cvrnr. 31483891 Филиал Randers Lupinvej 10 8920 Randers NV Филиал Дубай Jsl sss Свободная зона Дубай Объединенные Арабские Эмираты',
            'content_ua' => 'Advokatanpartsselskabet Adduco Телефон 46 92 47 99 Електронна пошта adduco@adduco.dk Cvrnr. 31483891 Відділення Randers Lupinvej 10 8920 Randers NV Відділення Дубай Jsl sss Вільна зона Дубай Об\'єднані Арабські Емірати',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        //end contexts

        //start words
        Setting::insert([
            'name' => 'name',
            'title_en' => 'Name',
            'title_dk' => 'navn',
            'title_ru' => 'название',
            'title_ua' => 'ім\'я',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'email',
            'title_en' => 'Email',
            'title_dk' => 'E-mail',
            'title_ru' => 'электронное письмо',
            'title_ua' => 'електронною поштою',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'password',
            'title_en' => 'Password',
            'title_dk' => 'adgangskode',
            'title_ru' => 'пароль',
            'title_ua' => 'пароль',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'retype_password',
            'title_en' => 'Retype password',
            'title_dk' => 'Genindtast kodeord',
            'title_ru' => 'Повторите ввод пароля',
            'title_ua' => 'повторно введіть пароль',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'login',
            'title_en' => 'Login',
            'title_dk' => 'Log på',
            'title_ru' => 'авторизоваться',
            'title_ua' => 'логін',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'register',
            'title_en' => 'Register',
            'title_dk' => 'Tilmeld',
            'title_ru' => 'регистр',
            'title_ua' => 'реєструвати',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'forgot_password',
            'title_en' => 'Forgot password',
            'title_dk' => 'glemt kodeord',
            'title_ru' => 'забыл пароль',
            'title_ua' => 'Забули пароль',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'logout',
            'title_en' => 'Logout',
            'title_dk' => 'Log ud',
            'title_ru' => 'Выйти',
            'title_ua' => 'Вийти',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'Submit',
            'title_en' => 'Submit',
            'title_dk' => 'Indsend',
            'title_ru' => 'представить',
            'title_ua' => 'Подати',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'forget_password_text',
            'title_en' => 'Forgot password',
            'title_dk' => 'glemt kodeord',
            'title_ru' => 'забыл пароль',
            'title_ua' => 'Забули пароль',
            'content_en' => 'If you forgot your password, you can recover your account by your email. we will send an email for you, please check your email and click on the confirm link.',
            'content_dk' => 'Hvis du har glemt din adgangskode, kan du gendanne din konto via din e-mail. vi sender en e-mail til dig, tjek din e-mail og klik på bekræftelseslinket.',
            'content_ru' => 'Если вы забыли пароль, вы можете восстановить свою учетную запись по электронной почте. мы отправим вам электронное письмо, проверьте свою электронную почту и нажмите на ссылку подтверждения.',
            'content_ua' => 'Якщо ви забули пароль, ви можете відновити свій рахунок електронною поштою. ми надішлемо вам електронний лист, перевірте електронну пошту та натисніть на посилання для підтвердження.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::insert([
            'name' => 'subject',
            'title_en' => 'Subject',
            'title_dk' => 'Emne',
            'title_ru' => 'Предмет',
            'title_ua' => 'Тема',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'subject',
            'title_en' => 'Subject',
            'title_dk' => 'Emne',
            'title_ru' => 'Предмет',
            'title_ua' => 'Тема',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'subject',
            'title_en' => 'Subject',
            'title_dk' => 'Emne',
            'title_ru' => 'Предмет',
            'title_ua' => 'Тема',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'subject',
            'title_en' => 'Subject',
            'title_dk' => 'Emne',
            'title_ru' => 'Предмет',
            'title_ua' => 'Тема',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'message',
            'title_en' => 'Message',
            'title_dk' => 'Besked',
            'title_ru' => 'Сообщение',
            'title_ua' => 'повідомлення',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'send',
            'title_en' => 'Send',
            'title_dk' => 'Sende',
            'title_ru' => 'послать',
            'title_ua' => 'Надіслати',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'set_a_password',
            'title_en' => 'Set a password',
            'title_dk' => 'Indstil en adgangskode',
            'title_ru' => 'Установить пароль',
            'title_ua' => 'Встановіть пароль',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'retype_password',
            'title_en' => 'Retype password',
            'title_dk' => 'Genindtast kodeord',
            'title_ru' => 'Повторите ввод пароля',
            'title_ua' => 'Повторно введіть пароль',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'sign_up_for_free',
            'title_en' => 'Sign up for free',
            'title_dk' => 'Gratis tilmelding',
            'title_ru' => 'Gratis tilmelding',
            'title_ua' => 'Зареєструйтесь безкоштовно',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'sign_up',
            'title_en' => 'Sign up',
            'title_dk' => 'Tilmelde',
            'title_ru' => 'зарегистрироваться',
            'title_ua' => 'Зареєструйтесь',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'log_in',
            'title_en' => 'Log in',
            'title_dk' => 'Log på',
            'title_ru' => 'Авторизоваться',
            'title_ua' => 'Увійдіть',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'welcome_back',
            'title_en' => 'Welcome back',
            'title_dk' => 'Velkommen tilbage',
            'title_ru' => 'Добро пожаловать',
            'title_ua' => 'З поверненням',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'new_ticket',
            'title_en' => 'New message',
            'title_dk' => 'Ny meddelelse',
            'title_ru' => 'Новое сообщение',
            'title_ua' => 'Нове повідомлення',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'tickets',
            'title_en' => 'Messages',
            'title_dk' => 'Beskeder',
            'title_ru' => 'Сообщения',
            'title_ua' => 'Повідомлення',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'welcome',
            'title_en' => 'Welcome',
            'title_dk' => 'Velkommen',
            'title_ru' => 'Добро пожаловать',
            'title_ua' => 'Ласкаво просимо',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        Setting::insert([
            'name' => 'this_is_your_client_area',
            'title_en' => 'this is your client area',
            'title_dk' => 'dette er dit klientområde',
            'title_ru' => 'это ваша клиентская зона',
            'title_ua' => 'це зона вашого клієнта',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'list_of_tickets',
            'title_en' => 'List of tickets',
            'title_dk' => 'Liste over billetter',
            'title_ru' => 'Список билетов',
            'title_ua' => 'Список квитків',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'content',
            'title_en' => 'Content',
            'title_dk' => 'Indhold',
            'title_ru' => 'Indhold',
            'title_ua' => 'Зміст',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'created_at',
            'title_en' => 'Created at',
            'title_dk' => 'Oprettet den',
            'title_ru' => 'Создан в',
            'title_ua' => 'Створено в',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'status',
            'title_en' => 'Status',
            'title_dk' => 'Status',
            'title_ru' => 'Положение дел',
            'title_ua' => 'Статус',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'answered',
            'title_en' => 'Answered',
            'title_dk' => 'Besvaret',
            'title_ru' => 'Ответил',
            'title_ua' => 'Відповів',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'closed',
            'title_en' => 'Closed',
            'title_dk' => 'Lukket',
            'title_ru' => 'Закрыто',
            'title_ua' => 'зачинено',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'waiting_for_an_answer',
            'title_en' => 'Waiting for an answer',
            'title_dk' => 'Venter på et svar',
            'title_ru' => 'В ожидании ответа',
            'title_ua' => 'Чекаючи відповіді',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'add_more_files',
            'title_en' => 'Add more files',
            'title_dk' => 'Tilføj flere filer',
            'title_ru' => 'Добавить еще файлы',
            'title_ua' => 'Додайте більше файлів',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'your_message_sent_successfully_please_wait_for_an_answer',
            'title_en' => 'Your message sent successfully. please wait for an answer.',
            'title_dk' => 'Din besked blev sendt. vent et svar.',
            'title_ru' => 'Ваше сообщение успешно отправлено. пожалуйста подождите ответа.',
            'title_ua' => 'Ваше повідомлення успішно надіслано. будь ласка, почекайте відповіді.',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'open_your_first_ticket',
            'title_en' => 'Open your first ticket',
            'title_dk' => 'Åbn din første besked',
            'title_ru' => 'Откройте свое первое сообщение',
            'title_ua' => 'Відкрийте своє перше повідомлення',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'chat_with',
            'title_en' => 'Chat with',
            'title_dk' => 'Chatte med',
            'title_ru' => 'Чат с',
            'title_ua' => 'Говорити з',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Setting::insert([
            'name' => 'phone',
            'title_en' => 'Phone',
            'title_dk' => 'telefon',
            'title_ru' => 'Телефон',
            'title_ua' => 'Телефон',
            'content_en' => null,
            'content_dk' => null,
            'content_ru' => null,
            'content_ua' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        // Setting::insert([
        //     'name' => 'client',
        //     'title_en' => 'Home',
        //     'title_dk' => 'Hjem',
        //     'title_ru' => 'ДОМ',
        //     'title_ua' => 'ДОМА',
        //     'content_en' => null,
        //     'content_dk' => null,
        //     'content_ru' => null,
        //     'content_ua' => null,
        //     'created_at' => now(),
        //     'updated_at' => now()
        // ]);
        //end words
        //start tickets
        //start tickets
    }
}