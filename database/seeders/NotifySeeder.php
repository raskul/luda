<?php

namespace Database\Seeders;

use App\Models\Notify;
use Illuminate\Database\Seeder;

class NotifySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Notify::insert([
            'name' => 'admin_email',
            'value' => 'samraskul@gmail.com',
        ]);
        Notify::insert([
            'name' => 'admin_send_email',
            'value' => 'sadeghsadegh21@gmail.com',
        ]);
        Notify::insert([
            'name' => 'admin_send_email_password',
            'value' => 'please change it',
        ]);
        Notify::insert([
            'name' => 'admin_answer_user_message',
            'value' => 'yes',
        ]);
        Notify::insert([
            'name' => 'new_user_register',
            'value' => 'yes',
        ]);
        Notify::insert([
            'name' => 'user_send_message_to_admin',
            'value' => 'yes',
        ]);
        
        
    }
}
