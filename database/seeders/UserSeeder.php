<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'name'=>'sam',
            'email' => 'samraskul@gmail.com', 
            'password' => '$2y$10$EKFhfDnn/4goo8yAkgb3j./xWpGlKREzHI.pDIA/KrZMyURcDrXi2', //MGF2t6XPE9BGRcjs
            'is_admin' => true,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        User::insert([
            'name' => 'luda',
            'email' => 'luda@luda.com',
            'password' => '$2y$10$EKFhfDnn/4goo8yAkgb3j./xWpGlKREzHI.pDIA/KrZMyURcDrXi2', //MGF2t6XPE9BGRcjs
            'is_admin' => true,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}