<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->unsigned();
            $table->mediumText('description')->nullable();
            $table->date('next_contact_day')->nullable();
            $table->mediumText('next_contact_note')->nullable();
            $table->string('phone', 191)->nullable();
            $table->string('phone2', 191)->nullable();
            $table->string('phone3', 191)->nullable();
            $table->string('phone4', 191)->nullable();
            $table->string('rule', 191)->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_infos');
    }
}
