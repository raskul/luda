<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('user_email', 191);
            $table->bigInteger('parent_id')->nullable();
            $table->string('name', 191)->nullable();
            $table->string('phone', 191)->nullable();
            $table->string('subject', 191)->nullable();
            $table->mediumText('content')->nullable();
            $table->Text('files')->nullable();
            $table->enum('status', ['Unread','Read','Answered','Finished Successful','Finished Unsuccessful']);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
