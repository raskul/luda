<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyUser extends Mailable
{
    use Queueable, SerializesModels;
        
    public $subject = 'No Reply';
    public $text = '';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()//$emailText)
    {
        // $this->text = $emailText;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this);
        $level = true;
        $introLines = [$this->text, ''];
        $outroLines = ['', ''];
        $actionText = '';
        return $this->from('example@exa.com')
            ->markdown('vendor.notifications.email', compact(
                'level',
                'introLines',
                'outroLines'
                // 'actionText',

            ))->subject($this->subject);
    }
}
