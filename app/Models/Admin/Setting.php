<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Setting
 * @package App\Models\Admin
 * @version November 17, 2020, 2:21 am UTC
 *
 * @property string $name
 * @property string $title_en
 * @property string $content_en
 * @property string $title_dk
 * @property string $content_dk
 * @property string $title_ru
 * @property string $content_ru
 * @property string $title_ua
 * @property string $content_ua
 */
class Setting extends Model
{

    public $table = 'settings';
    



    public $fillable = [
        'name',
        'title_en',
        'content_en',
        'title_dk',
        'content_dk',
        'title_ru',
        'content_ru',
        'title_ua',
        'content_ua'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'title_en' => 'string',
        'title_dk' => 'string',
        'title_ru' => 'string',
        'title_ua' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
