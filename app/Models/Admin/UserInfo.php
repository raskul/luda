<?php

namespace App\Models\Admin;

use App\Models\User;
use Eloquent as Model;

/**
 * Class UserInfo
 * @package App\Models\Admin
 * @version November 20, 2020, 7:13 am UTC
 *
 * @property integer $user_id
 * @property string $description
 * @property string $next_contact_day
 * @property string $next_contact_note
 * @property string $phone
 * @property string $phone2
 * @property string $phone3
 * @property string $phone4
 * @property string $rule
 */
class UserInfo extends Model
{

    public $table = 'user_infos';
    



    public $fillable = [
        'user_id',
        'description',
        'next_contact_day',
        'next_contact_note',
        'phone',
        'phone2',
        'phone3',
        'phone4',
        'rule'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'next_contact_day' => 'date',
        'phone' => 'string',
        'phone2' => 'string',
        'phone3' => 'string',
        'phone4' => 'string',
        'rule' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    
    
}
