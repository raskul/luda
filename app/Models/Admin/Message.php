<?php

namespace App\Models\Admin;

use App\Models\User;
use Eloquent as Model;

/**
 * Class Message
 * @package App\Models\Admin
 * @version November 18, 2020, 10:11 pm UTC
 *
 * @property integer $user_id
 * @property string $user_email
 * @property integer $parent_id
 * @property string $name
 * @property string $phone
 * @property string $subject
 * @property string $content
 * @property string $files
 * @property string $status
 */
class Message extends Model
{

    public $table = 'messages';
    



    public $fillable = [
        'user_id',
        'user_email',
        'parent_id',
        'name',
        'phone',
        'subject',
        'content',
        'files',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'user_email' => 'string',
        'parent_id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'subject' => 'string',
        'files' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    
}
