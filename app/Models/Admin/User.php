<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class User
 * @package App\Models\Admin
 * @version November 20, 2020, 12:23 pm UTC
 *
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property sting $password
 * @property boolean $is_admin
 * @property string $rememberToken
 */
class User extends Model
{

    public $table = 'users';
    



    public $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'is_admin',
        'rememberToken'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'is_admin' => 'boolean',
        'rememberToken' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    // public function user_info(){
    //     return $this->hasOne(UserInfo::class, 'user_id');
    // }
    public function user_info()
    {
        return $this->hasOne(UserInfo::class, 'user_id', 'id', 'users');
    }
    
}
