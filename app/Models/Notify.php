<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    protected $fillable = ['name', 'value'];
    use HasFactory;

    public function adminEmail()
    {
        return $this->where('name', 'admin_email')->pluck('value')->first();
    }
}
