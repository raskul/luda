<?php

namespace App\Jobs;

use App\Mail\NotifyUser;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class EmailTo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emailAddress;
    protected $text;
    protected $subject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emailAddress, $subject = 'No Reply', $text = 'Text is not available')
    {
        $this->emailAddress = $emailAddress;
        $this->text = $text;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notifyUser = new NotifyUser;
        $notifyUser->text = $this->text;
        $notifyUser->subject = $this->subject;

        $user = new User;
        $user->id = 66666;
        $user->email = $this->emailAddress;
        $user->name = 'sssss';
        $user->email_verified_at = now();
        $user->password = '$2y$10$EKFhfDnn/4goo8yAkgb3j./xWpGlKREzHI.pDIA/KrZMyURcDrXi2';
        $user->created_at = now();
        $user->updated_at = now();

        Mail::to($user)->send($notifyUser);
    }
}
