<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\UserDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateUserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Repositories\Admin\UserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Admin\User as AdminUser;
use App\Models\Admin\UserInfo;
use App\Models\User;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('admin.users.index');
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        // $user = $this->userRepository->create($input);

        $user = new User;
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->is_admin = $input['is_admin'];
        $user->password = "";
        $user->save();
        
        $user_info = UserInfo::orderBy('id', 'desc')->first();
        $user_info->description = $input['description'];
        $user_info->next_contact_note = $input['next_contact_note'];
        $user_info->next_contact_day = $input['next_contact_day'];
        $user_info->phone = $input['phone'];
        $user_info->phone2 = $input['phone2'];
        $user_info->phone3 = $input['phone3'];
        $user_info->phone4 = $input['phone4'];
        $user_info->rule = $input['rule'];
        $user_info->save();

        Flash::success('User saved successfully.');

        return redirect(route('admin.users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('admin.users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        // $user = $this->userRepository->find($id);

        // if (empty($user)) {
        //     Flash::error('User not found');

        //     return redirect(route('admin.users.index'));
        // }

        // $user = $this->userRepository->update($request->all(), $id);

        $input = $request->all();

        $user = User::find($id);
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->is_admin = $input['is_admin'];
        $user->save();

        $user_info = UserInfo::where('user_id', $id)->first();
        $user_info->description = $input['description'];
        $user_info->next_contact_note = $input['next_contact_note'];
        $user_info->next_contact_day = $input['next_contact_day'];
        $user_info->phone = $input['phone'];
        $user_info->phone2 = $input['phone2'];
        $user_info->phone3 = $input['phone3'];
        $user_info->phone4 = $input['phone4'];
        $user_info->rule = $input['rule'];
        $user_info->save();

        Flash::success('User updated successfully.');

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        // $user_info = UserInfo::where('user_id', $id)->first();
        // $user_info->delete();

        Flash::success('User deleted successfully.');

        return redirect(route('admin.users.index'));
    }
}
