<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Message;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Session;

class AdminDashboardController extends AppBaseController
{
    public function index()
    {
        return view('admin.adminDashboard');
    }

    public function message(){
        $tickets = Message::where('parent_id', null)->orderBy('created_at', 'desc')->paginate(40);
        // dd($tickets[1]);

        return view('admin.adminMessage', compact('tickets'));
    }
    
    public function messageChangeStatus(Request $request, $id){
        $message = Message::find($id);
        $message->status = $request->input('status');
        $message->save();
        Session::flash('success', 'message status updated successfully');

        return redirect()->back();
    }

    public function messageReplyGet($message_id){
        $message = Message::find($message_id);
        if($message->status == 'Unread'){
            $message->status = 'Read';
            $message->save();
        }
        // dd('hi');
        $messages = Message::where('id', $message_id)->orWhere('parent_id', $message_id)->orderBy('created_at')->take(300)->get();

        return view('admin.adminMessageReply', compact('messages'));

    }
}
