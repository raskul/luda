<?php

namespace App\Http\Controllers\Admin;

use Flash;
use Response;
use App\Models\User;
use App\Http\Requests\Admin;
use Illuminate\Http\Request;
use App\Models\Admin\UserInfo;
use Yajra\DataTables\Facades\DataTables;
use App\DataTables\Admin\UserInfoDataTable;
use App\Http\Controllers\AppBaseController;
use App\Repositories\Admin\UserInfoRepository;
use App\Http\Requests\Admin\CreateUserInfoRequest;
use App\Http\Requests\Admin\UpdateUserInfoRequest;

class UserInfoController extends AppBaseController
{
    /** @var  UserInfoRepository */
    private $userInfoRepository;

    public function __construct(UserInfoRepository $userInfoRepo)
    {
        $this->userInfoRepository = $userInfoRepo;
    }

    /**
     * Display a listing of the UserInfo.
     *
     * @param UserInfoDataTable $userInfoDataTable
     * @return Response
     */
    public function index(UserInfoDataTable $userInfoDataTable, Request $request)
    {
        return $userInfoDataTable->render('admin.user_infos.index');
    }

    /**
     * Show the form for creating a new UserInfo.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.user_infos.create');
    }

    /**
     * Store a newly created UserInfo in storage.
     *
     * @param CreateUserInfoRequest $request
     *
     * @return Response
     */
    public function store(CreateUserInfoRequest $request)
    {
        $input = $request->all();

        $userInfo = $this->userInfoRepository->create($input);

        Flash::success('User Info saved successfully.');

        return redirect(route('admin.userInfos.index'));
    }

    /**
     * Display the specified UserInfo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userInfo = $this->userInfoRepository->find($id);

        if (empty($userInfo)) {
            Flash::error('User Info not found');

            return redirect(route('admin.userInfos.index'));
        }

        return view('admin.user_infos.show')->with('userInfo', $userInfo);
    }

    /**
     * Show the form for editing the specified UserInfo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userInfo = $this->userInfoRepository->find($id);

        if (empty($userInfo)) {
            Flash::error('User Info not found');

            return redirect(route('admin.userInfos.index'));
        }

        return view('admin.user_infos.edit')->with('userInfo', $userInfo);
    }

    /**
     * Update the specified UserInfo in storage.
     *
     * @param  int              $id
     * @param UpdateUserInfoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserInfoRequest $request)
    {
        $userInfo = $this->userInfoRepository->find($id);

        if (empty($userInfo)) {
            Flash::error('User Info not found');

            return redirect(route('admin.userInfos.index'));
        }

        $userInfo = $this->userInfoRepository->update($request->all(), $id);

        Flash::success('User Info updated successfully.');

        return redirect(route('admin.userInfos.index'));
    }

    /**
     * Remove the specified UserInfo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userInfo = $this->userInfoRepository->find($id);

        if (empty($userInfo)) {
            Flash::error('User Info not found');

            return redirect(route('admin.userInfos.index'));
        }

        $this->userInfoRepository->delete($id);

        Flash::success('User Info deleted successfully.');

        return redirect(route('admin.userInfos.index'));
    }
}
