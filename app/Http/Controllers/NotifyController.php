<?php

namespace App\Http\Controllers;

use App\Models\Notify;
use Illuminate\Http\Request;
use Session;

class NotifyController extends Controller
{
    public function showForm()
    {
        $admin_email = Notify::where('name', 'admin_email')->pluck('value')->first();
        $admin_send_email = Notify::where('name', 'admin_send_email')->pluck('value')->first();
        // $admin_send_email_password = Notify::where('name', 'admin_send_email_password')->pluck('value')->first();
        $new_user_register = Notify::where('name', 'new_user_register')->pluck('value')->first();
        $user_send_message_to_admin = Notify::where('name', 'user_send_message_to_admin')->pluck('value')->first();
        $admin_answer_user_message = Notify::where('name', 'admin_answer_user_message')->pluck('value')->first();

        return view(
            'admin.adminNotification',
            compact(
                'admin_email',
                'admin_send_email',
                'new_user_register',
                'user_send_message_to_admin',
                'admin_answer_user_message',

            )
        );
    }

    public function showFormPost(Request $request){
        
        $admin_send_email = $request->input('admin_send_email');
        updateDotEnv('MAIL_USERNAME', $admin_send_email);
        Notify::where('name', 'admin_send_email')->first()->update(['value' => $admin_send_email]);

        $admin_send_email_password = $request->input('admin_send_email_password');
        if($admin_send_email_password != '********'){
            updateDotEnv('MAIL_PASSWORD', $admin_send_email_password);
            Notify::where('name', 'admin_send_email_password')->first()->update(['value' => $admin_send_email_password]);
        }

        Notify::where('name', 'admin_email')->first()->update(['value' => $request->input('admin_email')]);

        if($request->input('new_user_register') == 'yes'){
            Notify::where('name', 'new_user_register')->first()->update(['value' => 'yes']);
        }else{
            Notify::where('name', 'new_user_register')->first()->update(['value' => 'no']);
        }

        if ($request->input('user_send_message_to_admin') == 'yes') {
            Notify::where('name', 'user_send_message_to_admin')->first()->update(['value' => 'yes']);
        } else {
            Notify::where('name', 'user_send_message_to_admin')->first()->update(['value' => 'no']);
        }

        if ($request->input('admin_answer_user_message') == 'yes') {
            Notify::where('name', 'admin_answer_user_message')->first()->update(['value' => 'yes']);
        } else {
            Notify::where('name', 'admin_answer_user_message')->first()->update(['value' => 'no']);
        }

        Session::flash('success', 'Your changes saved.');

        return redirect()->back();
    }
}
