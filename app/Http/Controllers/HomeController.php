<?php

namespace App\Http\Controllers;

use Session;
use App\Jobs\EmailTo;
use App\Models\Notify;
use Illuminate\Http\Request;
use App\Models\Admin\Message;
use App\Models\Admin\Setting;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Console\Input\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()//home
    {
        $lang = 'en';
        if(Session::has('languages')){
            $lang = Session::get('languages');
        }
        $contentAndTitle = Setting::where('name', 'home_text')->get(['title_'.$lang, 'content_'.$lang])->first();
        $title = $contentAndTitle['title_'.$lang];
        $content = $contentAndTitle['content_'.$lang];
        //

        return view('index.home', compact('content', 'title'));
    }

    public function about(){
        $lang = 'en';
        if (Session::has('languages')) {
            $lang = Session::get('languages');
        }
        $contentAndTitle = Setting::where('name', 'about_us_text')->get(['title_' . $lang, 'content_' . $lang])->first();
        $title = $contentAndTitle['title_' . $lang];
        $content = $contentAndTitle['content_' . $lang];
        //


        return view('index.about', compact('title', 'content'));
    }
    
    public function clients(){
        $lang = 'en';
        if (Session::has('languages')) {
            $lang = Session::get('languages');
        }
        $contentAndTitle = Setting::where('name', 'clients_text')->get(['title_' . $lang, 'content_' . $lang])->first();
        $title = $contentAndTitle['title_' . $lang];
        $content = $contentAndTitle['content_' . $lang];
        
        if(Auth::check()){
            return redirect()->route('clients.ticket');
        }

        return view('index.client', compact('title', 'content'));
    }

    public function forgetPassword(){
        $lang = 'en';
        if (Session::has('languages')) {
            $lang = Session::get('languages');
        }
        $contentAndTitle = Setting::where('name', 'forget_password_text')->get(['title_' . $lang, 'content_' . $lang])->first();
        $title = $contentAndTitle['title_' . $lang];
        $content = $contentAndTitle['content_' . $lang];
        //


        return view('index.forgetPassword', compact('title', 'content')); 
    }

    public function vacancies()
    {
        $lang = 'en';
        if (Session::has('languages')) {
            $lang = Session::get('languages');
        }
        $contentAndTitle = Setting::where('name', 'vacancies_text')->get(['title_' . $lang, 'content_' . $lang])->first();
        $title = $contentAndTitle['title_' . $lang];
        $content = $contentAndTitle['content_' . $lang];
        //


        return view('index.vacancies', compact('title', 'content'));
    }

    public function contact()
    {
        $lang = 'en';
        if (Session::has('languages')) {
            $lang = Session::get('languages');
        }
        $contentAndTitle = Setting::where('name', 'contact_text')->get(['title_' . $lang, 'content_' . $lang])->first();
        $title = $contentAndTitle['title_' . $lang];
        $content = $contentAndTitle['content_' . $lang];

        return view('index.contact', compact('title', 'content'));
    }

    public function messageStore(Request $request){
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $subject = $request->input('subject');
        $content = $request->input('content');
        if(strlen($content) < 1){
            Session::flash('danger', 'The content of the message should be at least 1 characters.');
            return redirect()->back();
        }

        $message = new Message;
        $message->name = $name;
        $message->user_email = $email;
        $message->phone = $phone;
        $message->subject = $subject;
        $message->content = $content;

        if(Auth::check()){
            $message->user_id = Auth::user()->id;
            $message->user_email = Auth::user()->email;
            $message->name = Auth::user()->name;
            $message->phone =Auth::user()->user_info->phone;
        }

        $message->save();
        
        //start send email.
        //send email to user:
        $notify_admin_answer_user_message = Notify::where('name', 'admin_answer_user_message')->pluck('value')->first();
        if ($notify_admin_answer_user_message == 'yes') {
            if ((Auth::check()) && (Auth::user()->is_admin)) {
                EmailTo::dispatch($message->user_email, 'No Reply', $message->content . printFilesWithFullUrl($message->files));
            } else {
                EmailTo::dispatch($message->user_email, 'No Reply', 'We Received Your Message. wait for an answer.<br><br>this is your message: <p style="font-size:small">' . $message->content . printFilesWithFullUrl($message->files) . "</p>");
            }
        }
        //send email to admin for notification to understand there is a new message:
        $user_send_message_to_admin = Notify::where('name', 'user_send_message_to_admin')->pluck('value')->first();
        if ($user_send_message_to_admin == 'yes') {
            $n = new Notify;
            $adminEmail = $n->adminEmail();
            if ((!Auth::check() ) || (!Auth::user()->is_admin)) {
                EmailTo::dispatch($adminEmail, 'New message. ' . $message->subject, "You have a new message.<br><br>From_email:<br> $message->user_email<br><br>Phone:<br>" . $message->phone . "<br><br> Subject:<br>$message->subject<br><br>Content:<br>" .  $message->content . printFilesWithFullUrl($message->files));
            }
        }
        //end send email.

        Session::flash('success', 'Your message send successfully. thanks for messaging us');

        return redirect()->back();
    }
}