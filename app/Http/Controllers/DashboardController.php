<?php

namespace App\Http\Controllers;

use App\Jobs\EmailTo;
use App\Models\Admin\Message;
use App\Models\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class DashboardController extends Controller
{
    // public function dashboard()
    // {
    //     return view('dashboard.dashboard');
    // }

    public function clientsTicket()
    {
        $user_id = Auth::user()->id;
        // $email = Auth::user()->email;
        $tickets = Message::where('user_id', $user_id)->where('parent_id', null)->orderBy('created_at', 'desc')->get(['id', 'created_at', 'subject', 'content', 'status'])->take(50);
        // dd($tickets);

        return view('dashboard.clientsTicket', compact('tickets'));
    }

    public function clientsTicketNewGet()
    {
        return view('dashboard.clientsTicketNew');
    }

    public function clientsTicketNewPost(Request $request)
    {
        $subject = $request->input('subject');
        $content = $request->input('content');
        $phone = $request->input('phone');

        if (!Auth::check()) { //if user not logged in.
            Session::flash('error', 'You should login.');
            return redirect()->route('clients');
        }

        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        $parent_id = null;
        $name = Auth::user()->name;
        $status = 'Unread';

        $files = storeFilesAsString($request);

        $message = new Message;
        $message->user_id = $user_id;
        $message->user_email = $user_email;
        $message->parent_id = $parent_id;
        $message->name = $name;
        $message->phone = $phone;
        $message->status = $status;
        $message->subject = $subject;
        $message->content = $content;
        $message->files = $files;

        if (Auth::check()) {
            $message->user_id = Auth::user()->id;
            $message->user_email = Auth::user()->email;
            $message->name = Auth::user()->name;
            $message->phone = Auth::user()->user_info->phone;
        }
        
        $message->save();

        //start send email.
        //send email to user:
        $notify_admin_answer_user_message = Notify::where('name', 'admin_answer_user_message')->pluck('value')->first();
        if ($notify_admin_answer_user_message == 'yes') {
            if ((Auth::check()) && (Auth::user()->is_admin)) {
                EmailTo::dispatch($message->user_email, 'No Reply', $message->content . printFilesWithFullUrl($message->files));
            } else {
                EmailTo::dispatch($message->user_email, 'No Reply', 'We Received Your Message. wait for an answer.<br><br>this is your message: <p style="font-size:small">' . $message->content . printFilesWithFullUrl($message->files) . "</p>");
            }
        }
        //send email to admin for notification to understand there is a new message:
        $user_send_message_to_admin = Notify::where('name', 'user_send_message_to_admin')->pluck('value')->first();
        if ($user_send_message_to_admin == 'yes') {
            $n = new Notify;
            $adminEmail = $n->adminEmail();
            if ((!Auth::check()) || (!Auth::user()->is_admin)) {
                EmailTo::dispatch($adminEmail, 'New message. ' . $message->subject, "You have a new message.<br><br>From_email:<br> $message->user_email<br><br>Phone:<br>" . $message->phone . "<br><br> Subject:<br>$message->subject<br><br>Content:<br>" .  $message->content . printFilesWithFullUrl($message->files));
            }
        }
        //end send email.

        Session::flash('success', 'Your Message sent successfully. please wait for answer.');

        return redirect()->route('clients.ticket');
    }

    public function clientsTicketSingle($ticketId)
    {
        $messages = Message::where('id', $ticketId)->orWhere('parent_id', $ticketId)->orderBy('created_at')->take(200)->get();

        //start check owner of the message
        $messageTemp = Message::find($ticketId);
        if ($messageTemp->user->id != Auth::user()->id) {
            die("you're not authorized to see this message.");
        }
        //end check owner of the message

        return view('dashboard.clientsTicketSingle', compact('messages'));
    }

    public function clientsTicketAnswerPost(Request $request, $parent_id)
    {
        $subject = null;
        $content = $request->input('content');

        if (!Auth::check()) { //if user not logged in.
            Session::flash('error', 'You should login.');
            return redirect()->route('clients');
        }

        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        // $parent_id came from url
        $name = Auth::user()->name;
        $phone = null;

        //start change parent status
        $status = 'Unread';
        if (Auth::user()->is_admin) {
            $status = 'Answered';
        }
        $parentMessage = Message::find($parent_id);
        $parentMessage->status = $status;
        $parentMessage->save();
        //end change parent status

        $status = 'Unread'; //for this message

        $files = storeFilesAsString($request);

        $message = new Message;
        $message->user_id = $user_id;
        $message->user_email = $user_email;
        $message->parent_id = $parent_id;
        $message->name = $name;
        $message->phone = $phone;
        $message->status = $status;
        $message->subject = $subject;
        $message->content = $content;
        $message->files = $files;
        $message->save();

        //start send email.
        //send email to user:
        $notify_admin_answer_user_message = Notify::where('name', 'admin_answer_user_message')->pluck('value')->first();
        if ($notify_admin_answer_user_message == 'yes') {
            $parentMessageEmail = $parentMessage->user_email;
            if (Auth::user()->is_admin) {
                EmailTo::dispatch($parentMessageEmail, 'No Reply', $message->content . printFilesWithFullUrl($message->files));
            } else {
                EmailTo::dispatch($parentMessageEmail, 'No Reply', 'We Received Your Message. wait for an answer.<br><br>this is your message: <p style="font-size:small">' . $message->content . printFilesWithFullUrl($message->files) . "</p>");
            }
        }
        //send email to admin for notification to understand there is a new message:
        $user_send_message_to_admin = Notify::where('name', 'user_send_message_to_admin')->pluck('value')->first();
        if ($user_send_message_to_admin == 'yes') {
            $n = new Notify;
            $adminEmail = $n->adminEmail();
            if (!Auth::user()->is_admin) {
                EmailTo::dispatch($adminEmail, 'New message. ' . $parentMessage->subject, "You have a new message.<br><br>From_email:<br> $message->user_email<br><br>Phone:<br>" . $parentMessage->user->user_info->phone . "<br><br> Subject:<br>$parentMessage->subject<br><br>Content:<br>" .  $message->content . printFilesWithFullUrl($message->files));
            }
        }
        //end send email.

        Session::flash('success', 'Your Message sent successfully. please wait for answer.');

        // return redirect()->route('clients.ticket.single', $parent_id);
        return redirect()->back();
    }
}
