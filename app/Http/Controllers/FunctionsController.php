<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class FunctionsController extends Controller
{
    public function language($lang){
        if (Session::has('languages')) {
            Session::forget('languages');
        }
        Session::put('languages', $lang);
        return redirect()->back();
    }
}
