<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if((Auth::user()) && (Auth::user()->is_admin)){
            return $next($request);
        }else{
            die('You don\'t have permission to see this page. You\'re not admin. if you\'re Admin please be sure that you\'re logged in.');
        }
    }
}
