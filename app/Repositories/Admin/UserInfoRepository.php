<?php

namespace App\Repositories\Admin;

use App\Models\Admin\UserInfo;
use App\Repositories\BaseRepository;

/**
 * Class UserInfoRepository
 * @package App\Repositories\Admin
 * @version November 20, 2020, 7:13 am UTC
*/

class UserInfoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'description',
        'next_contact_day',
        'next_contact_note',
        'phone',
        'phone2',
        'phone3',
        'phone4',
        'rule'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserInfo::class;
    }
}
