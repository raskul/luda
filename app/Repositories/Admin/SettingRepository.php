<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Setting;
use App\Repositories\BaseRepository;

/**
 * Class SettingRepository
 * @package App\Repositories\Admin
 * @version November 17, 2020, 2:21 am UTC
*/

class SettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'title_en',
        'content_en',
        'title_dk',
        'content_dk',
        'title_ru',
        'content_ru',
        'title_ua',
        'content_ua'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Setting::class;
    }
}
