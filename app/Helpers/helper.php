<?php

use App\Models\Admin\Setting;

//store the Files that come for message in the public folder
//return $files(string) to store it in the $message->files
// Usage: 
// $files = storeFilesAsString($request); 
// $message->files = $files;
// $message->save();
function storeFilesAsString($request)
{
    $files = "";
    for ($i = 1; $i < 200; $i++) {
        if ($request->hasFile("file$i")) {
            request()->validate([
                "file$i" => 'mimes:png,jpg,jpeg,mp3,mp4,avi,mkv,txt,pdf,doc,docx|max:16000'
            ]);
            $f = $request->file("file$i");
            $n = time() . rand(100000, 999999) . $f->getClientOriginalName();
            $f->move(public_path() . '/uploads/messages', $n);
            $filepath = '/uploads/messages/' . $n;
            $files .= $filepath;
        } else if ($i > 4) {
            break;
        }
    }

    return $files;
}

// "Files" in message is an array. to print it line by line you can use this function.
// it will return all the attachments of message as link inside a div
// Usage: {{ printFiles($message->files); }}
function printFiles($files)
{ //file should be message->files
    $filesTemp = explode("/uploads/messages/", $files);
    unset($filesTemp[0]);
    $links = "<div>";
    $i = 1;
    foreach ($filesTemp as $file) {
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $link = '<a href="/uploads/messages/' . $file . '">Attached' . $i . '.' . $fileExtension . '</a><br />';
        $links .= $link;
        $i++;
    }
    $links .= "</div>";
    return $links;
}

// "Files" in message is an array. to print it line by line you can use this function.
// it will return all the attachments of message as link inside a div
// Usage: {{ printFilesWithFullUrl($message->files); }} 
function printFilesWithFullUrl($files)
{ //file should be message->files
    $filesTemp = explode("/uploads/messages/", $files);
    unset($filesTemp[0]);
    $links = "<div>";
    $i = 1;
    foreach ($filesTemp as $file) {
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $link = '<a href="' . env('APP_URL') . '/uploads/messages/' . $file . '">Attached' . $i . '.' . $fileExtension . '</a><br />';
        $links .= $link;
        $i++;
    }
    $links .= "</div>";
    return $links;
}

/**return all the words in database related to language session to an Array**/
// usage in blade:
// @php
//     $t = translateAllWordsFromTitle();
//     dd($t);
// @endphp
function translateAllWordsFromTitle()
{
    $lang = Session::get('languages');
    $arrayOfWords = Setting::whereNotNull('title_' . $lang)->get(['name', 'title_' . $lang])->toArray();

    $array = [];
    foreach ($arrayOfWords as $a) {
        $t1 = $a['name'];
        $t2 = $a['title_' . $lang];
        $array[$t1] = $t2;
    }

    return $array;
}

//translate specific world form content;
function translateContent($wordName)
{
    $lang = Session::get('languages');
    $translatedWord = Setting::where('name', $wordName)->pluck('content_' . $lang)->first();

    return $translatedWord;
}

//edit .env file for email smtp configuration
function updateDotEnv($key, $newValue, $delim = '')
{
    $path = base_path('.env');
    // get old value from current env
    $oldValue = env($key);
    // was there any change?
    if ($oldValue === $newValue) {
        return;
    }
    // rewrite file content with changed data
    if (file_exists($path)) {
        // replace current value with new value 
        file_put_contents(
            $path,
            str_replace(
                $key . '=' . $delim . $oldValue . $delim,
                $key . '=' . $delim . $newValue . $delim,
                file_get_contents($path)
            )
        );
    }
}
